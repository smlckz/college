/* Grading system */
#include <stdio.h>

int main(void)
{
	int n1, n2, n3, n4;
	float avg;
	printf("Grading system\n\n");
	printf("Enter marks obtained in four subjects: ");
	scanf("%d%d%d%d", &n1, &n2, &n3, &n4);
	avg = (float)(n1 + n2 + n3 + n4)/4;
	printf("The average of the marks is %f\n", avg);
	if (avg >= 80)
		printf("The student has grade A.\n");
	if (avg >= 60 && avg < 80)
		printf("The student has grade B.\n");
	if (avg >= 40 && avg < 60)
		printf("The student has grade C.\n");
	if (avg < 40)
		printf("The student has failed.\n");
	return 0;
}

/*
Output:
Set 1:
Grading system

Enter marks obtained in four subjects: 81
85
86
91
The average of the marks is 85.750000
The student has grade A.

Set 2:
Grading system

Enter marks obtained in four subjects: 86 75 71 42
The average of the marks is 68.500000
The student has grade B.

Set 3:
Grading system

Enter marks obtained in four subjects: 45
55
51
42
The average of the marks is 48.250000
The student has grade C.

Set 4:
Grading system

Enter marks obtained in four subjects: 25
31
40 32
The average of the marks is 32.000000
The student has failed.

*/

