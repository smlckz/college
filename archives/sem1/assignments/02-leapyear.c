/* Leap year */
#include <stdio.h>

int main(void)
{
	int year;
	printf("Enter a year: ");
	scanf("%d", &year);
	if (year <= 1200 || year >= 9999) {
		printf("Invalid year.\n");
		return 0;
	}
	if ((year % 400 == 0) || (year % 100 != 0 && year % 4 == 0))
		printf("%d is a leap year.\n", year);
	else
		printf("%d is not a leap year.\n", year);
	return 0;
}

/*
Output:
Set 1:
Enter a year: 1300
1300 is not a leap year.

Set 2:
Enter a year: 1600
1600 is a leap year.

Set 3:
Enter a year: 2008
2008 is a leap year.

Set 4:
Enter a year: 2003
2003 is not a leap year.

Set 5:
Enter a year: 1100
Invalid year.

Set 6:
Enter a year: 10000
Invalid year.

*/

