/* Quadratic equation and the nature of the roots */

#include <stdio.h>
#include <math.h>

int main(void)
{
	float a, b, c, discr, x1, x2;
	printf("Finding the roots and the nature of them of quadratic equation\n"
		"\t\tax^2+bx+c=0\n\n");
	printf("Enter a: ");
	scanf("%f", &a);
	printf("Enter b: ");
	scanf("%f", &b);
	printf("Enter c: ");
	scanf("%f", &c);
	if (a == 0) {
		printf("In a quadratic equation, a can not be zero.\n");
		return 0;
	}
	discr = b * b - 4 * a * c;
	if (discr < 0) {
		printf("The roots of this equation are imaginary.\n");
		return 0;
	}
	if (discr == 0) {
		printf("The roots of this equation are real and equal.\n");
	}
	if (discr > 0) {
		printf("The roots of this equation are real and distinct.\n");
	}
	x1 = (-b + sqrt(discr)) / (2 * a);
	x2 = (-b - sqrt(discr)) / (2 * a);
	printf("The roots are: %f and %f\n", x1, x2);
	return 0;
}

/*
Output:
Set 1:
Finding the roots and the nature of them of quadratic equation
		ax^2+bx+c=0

Enter a: 0
Enter b: -1
Enter c: 2
In a quadratic equation, a can not be zero.

Set 2:
Finding the roots and the nature of them of quadratic equation
		ax^2+bx+c=0

Enter a: -1
Enter b: 4
Enter c: -5
The roots of this equation are imaginary.

Set 3:
Finding the roots and the nature of them of quadratic equation
		ax^2+bx+c=0

Enter a: 2.5
Enter b: -5
Enter c: 2.5
The roots of this equation are real and equal.
The roots are: 1.000000 and 1.000000

Set 4:
Finding the roots and the nature of them of quadratic equation
		ax^2+bx+c=0

Enter a: -1.23
Enter b: 45.6
Enter c: 789
The roots of this equation are real and distinct.
The roots are: -12.849214 and 49.922382

*/

