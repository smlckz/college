/* Sum and product of the digits of given number */

#include <stdio.h>

int main(void)
{
	int n, m, r, sum, prod;
	printf("Sum and product of the digits of a given number\n\n");
	printf("Enter a number: ");
	scanf("%d", &n);
	m = n;
	sum = 0;
	prod = 1;
	do {
		r = m % 10;
		m = m / 10;
		sum = sum + r;
		prod = prod * r;
	} while (m > 0);
	printf("Sum of digits of %d is %d\n", n, sum);
	printf("Product of digits of %d is %d\n", n, prod);
	return 0;
}

/*
Output:
Set 1:
Sum and product of the digits of a given number

Enter a number: 0
Sum of digits of 0 is 0
Product of digits of 0 is 0

Set 2:
Sum and product of the digits of a given number

Enter a number: 123456
Sum of digits of 123456 is 21
Product of digits of 123456 is 720

Set 3:
Sum and product of the digits of a given number

Enter a number: 123456789
Sum of digits of 123456789 is 45
Product of digits of 123456789 is 362880

*/

