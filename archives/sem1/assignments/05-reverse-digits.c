/* Print the digits of given number in reverse order */

#include <stdio.h>

int main(void)
{
	int n, m, r, sum, prod;
	printf("Print the digits of given number in reverse order\n\n");
	printf("Enter a number: ");
	scanf("%d", &n);
	m = n;
	printf("%d printed in reverse order is: ", n);
	do {
		r = m % 10;
		m = m / 10;
		printf("%d", r);
	} while (m > 0);
	printf("\n");
	return 0;
}

/*
Output:
Set 1:
Print the digits of given number in reverse order

Enter a number: 0
0 printed in reverse order is: 0

Set 2:
Print the digits of given number in reverse order

Enter a number: 123456
123456 printed in reverse order is: 654321

Set 3:
Print the digits of given number in reverse order

Enter a number: 541563249
541563249 printed in reverse order is: 942365145

*/

