/* Check whether a given number is palindromic or not */

#include <stdio.h>

int main(void)
{
	int n, m, p, r;
	printf("Check whether a given number is palindromic\n\n");
	printf("Enter a number: ");
	scanf("%d", &n);
	m = n;
	p = 0;
	while (m > 0) {
		r = m % 10;
		m = m / 10;
		p = p * 10 + r;
	}
	if (p == n) {
		printf("%d is a palindromic number.\n", n);
	} else {
		printf("%d is not a palindromic number.\n", n);
	}
	return 0;
}

/*
Output:
Set 1:
Check whether a given number is palindromic

Enter a number: 0
0 is a palindromic number.

Set 2:
Check whether a given number is palindromic

Enter a number: 123456
123456 is not a palindromic number.

Set 3:
Check whether a given number is palindromic

Enter a number: 1234321
1234321 is a palindromic number.

*/

