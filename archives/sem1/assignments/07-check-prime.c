#include <stdio.h>
#include <stdbool.h>

bool is_prime(int n)
{
	int i;
	for (i = 2; i < n; i++) {
		if (n % i == 0) return false;
	}
	return true;
}

int main(void)
{
	int i, n;
	printf("Check whether a given number is prime or not\n\n");
	printf("Enter a number: ");
	scanf("%d", &n);
	if (n < 2) {
		printf("Invalid input, it must be an integer greater than 2\n");
		return 0;
	}
	printf("%d is%s a prime number.\n", n, is_prime(n) ? "" : " not");
	return 0;
}

/*
Output:
Set 1:
Check whether a given number is prime or not

Enter a number: 100
100 is not a prime number.

Set 2:
Check whether a given number is prime or not

Enter a number: 97
97 is a prime number.

Set 3:
Check whether a given number is prime or not

Enter a number: 2
2 is a prime number.

Set 4:
Check whether a given number is prime or not

Enter a number: 1
Invalid input, it must be an integer greater than 2


*/

