/* Print the prime factorization of a given number */

#include <stdio.h>

int main(void)
{
	int n, m, i;
	printf("Print the prime factorization of a given number\n\n");
	printf("Enter a number: ");
	scanf("%d", &n);
	printf("The prime factors of %d are: ", n);
	m = n;
	while (m > 1) {
		for (i = 2; i <= m; i++) {
			while (m % i == 0) {
				printf("%d ", i);
				m = m / i;
			}
		}
	}
	printf("\n");
	return 0;
}

/*
Output:
Set 1:
Print the prime factorization of a given number

Enter a number: 2
The prime factors of 2 are: 2 

Set 2:
Print the prime factorization of a given number

Enter a number: 120
The prime factors of 120 are: 2 2 2 3 5 

Set 3:
Print the prime factorization of a given number

Enter a number: 97
The prime factors of 97 are: 97 

*/

