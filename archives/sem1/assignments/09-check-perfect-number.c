/* Check whether a given number is a perfect number */

#include <stdio.h>

int main(void)
{
	int n, sum, i, j, isprime;
	printf("Check whether a given number is a perfect number\n\n");
	printf("Enter a number: ");
	scanf("%d", &n);
	sum = 0;
	for (i = 1; i < n; i++) {
		if (n % i == 0) {
			sum += i;
		}
	}
	if (n == sum) {
		printf("%d is a perfect number.\n", n);
	} else {
		printf("%d is not a perfect number.\n", n);
	}
	return 0;
}


/*
Output:
Set 1:
Check whether a given number is a perfect number

Enter a number: 6
6 is a perfect number.

Set 2:
Check whether a given number is a perfect number

Enter a number: 10
10 is not a perfect number.

Set 3:
Check whether a given number is a perfect number

Enter a number: 28
28 is a perfect number.

Set 4:
Check whether a given number is a perfect number

Enter a number: 100
100 is not a perfect number.

*/

