/* Calculate the highest common factor and lowest common multiplier of two numbers */

#include <stdio.h>

int main(void)
{
	int a, b, m, n, t;
	printf("Calculate the HCF and LCM of two numbers\n\n");
	printf("Enter two numbers: ");
	scanf("%d%d", &a, &b);
	m = a;
	n = b;
	while (n > 0) {
		t = n;
		n = m % n;
		m = t;
	}
	printf("The HCF of %d and %d is %d\n", a, b, m);
	printf("The LCM of %d and %d is %d\n", a, b, (a*b)/m);
	return 0;
}

/*
Output:
Set 1:
Calculate the HCF and LCM of two numbers

Enter two numbers: 12 16
The HCF of 12 and 16 is 4
The LCM of 12 and 16 is 48

Set 2:
Calculate the HCF and LCM of two numbers

Enter two numbers: 112 67
The HCF of 112 and 67 is 1
The LCM of 112 and 67 is 7504

Set 3:
Calculate the HCF and LCM of two numbers

Enter two numbers: 1 2
The HCF of 1 and 2 is 1
The LCM of 1 and 2 is 2

Set 4:
Calculate the HCF and LCM of two numbers

Enter two numbers: 123 456
The HCF of 123 and 456 is 3
The LCM of 123 and 456 is 18696

*/

