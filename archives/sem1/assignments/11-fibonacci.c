/* Print first n fibonacci numbers */

#include <stdio.h>

int main(void)
{
	int a, b, i, n, t;
	printf("Print first n fibonacci number\n\n");
	printf("Enter a number: ");
	scanf("%d", &n);
	a = 1; b = 0;
	printf("The first %d fibonacci numbers are: ", n);
	for (i = 1; i <= n; i++) {
		printf("%d ", b);
		t = a;
		a = a + b;
		b = t;
	}
	printf("\n");
	return 0;
}

/*
Output:
Set 1:
Print first n fibonacci number

Enter a number: 5
The first 5 fibonacci numbers are: 0 1 1 2 3 

Set 2:
Print first n fibonacci number

Enter a number: 10
The first 10 fibonacci numbers are: 0 1 1 2 3 5 8 13 21 34 

Set 3:
Print first n fibonacci number

Enter a number: 20
The first 20 fibonacci numbers are: 0 1 1 2 3 5 8 13 21 34 55 89 144 233 377 610 987 1597 2584 4181 

*/

