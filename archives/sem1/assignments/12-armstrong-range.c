#include <math.h>
#include <stdio.h>
#include <stdbool.h>

bool is_armstrong(int n)
{
	int m = n, r, s = 0;
	float c = ceil(log10(n));
	while (m > 0) {
		r = m % 10;
		m /= 10;
		s += pow(r, c);
	}
	return n == s;
}

int main(void)
{
	int i, l, h;
	printf("Print Armstrong Numbers in a given range\n\n");
	printf("Enter the range: ");
	scanf("%d%d", &l, &h);
	if (h < l) {
		printf("Invalid range\n");
		return 0;
	}
	printf("Armstrong numbers between %d and %d are: ", l, h);
	for (i = l; i <= h; i++) {
		if (is_armstrong(i)) printf("%d ", i);
	}
	printf("\n");
	return 0;
}

/*
Output:
Set 1:
Print Armstrong Numbers in a given range

Enter the range: 100 1
Invalid range

Set 2:
Print Armstrong Numbers in a given range

Enter the range: 1 100
Armstrong numbers between 1 and 100 are: 1 2 3 4 5 6 7 8 9 

Set 3:
Print Armstrong Numbers in a given range

Enter the range: 100 10000
Armstrong numbers between 100 and 10000 are: 153 370 371 407 1634 8208 9474 

Set 4:
Print Armstrong Numbers in a given range

Enter the range: 10000 1000000
Armstrong numbers between 10000 and 1000000 are: 54748 92727 93084 548834 

*/

