#include <stdio.h>

int main(void)
{
	int i, j, h, f;
	printf("Enter a height: ");
	scanf("%d", &h);
	for (i = 1; i <= h; i++) {
		f = i % 2;
		for (j = 1; j <= i; j++) {
			printf("%d", f);
			f = !f;
		}
		printf("\n");
	}
	return 0;
}

/*
Output:
Set 1:
Enter a height: 5
1
01
101
0101
10101

Set 2:
Enter a height: 6
1
01
101
0101
10101
010101

Set 3:
Enter a height: 8
1
01
101
0101
10101
010101
1010101
01010101

Set 4:
Enter a height: 11
1
01
101
0101
10101
010101
1010101
01010101
101010101
0101010101
10101010101


*/

