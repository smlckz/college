#include <stdio.h>

int main(void)
{
	int i, j, h;
	printf("Enter a height: ");
	scanf("%d", &h);
	for (i = 1; i <= h; i++) {
		for (j = 1; j <= h - i; j++) {
			printf(" ");
		}
		for (j = 1; j <= 2 * i - 1; j++) {
			printf("*");
		}
		printf("\n");
	}
	return 0;
}

/*
Output:
Set 1:
Enter a height: 3
  *
 ***
*****

Set 2:
Enter a height: 5
    *
   ***
  *****
 *******
*********

Set 3:
Enter a height: 7
      *
     ***
    *****
   *******
  *********
 ***********
*************

*/

