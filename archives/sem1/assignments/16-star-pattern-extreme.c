#include <stdio.h>

int main(void)
{
	int i, j, h;
	printf("Enter a height: ");
	scanf("%d", &h);
	for (i = 1; i <= h; i++) {
		for (j = 1; j <= i; j++) {
			printf("*");
		}
		for (j = 1; j < 2 * h - 2 * i; j++) {
			printf(" ");
		}
		for (j = 1; j <= i; j++) {
			if (i == h && i == j) break;
			printf("*");
		}
		printf("\n");
	}
	return 0;
}

/*
Output:
Set 1:
Enter a height: 4
*      *
**    **
***  ***
********

Set 2:
Enter a height: 5
*        *
**      **
***    ***
****  ****
**********

Set 3:
Enter a height: 6
*          *
**        **
***      ***
****    ****
*****  *****
************

*/

