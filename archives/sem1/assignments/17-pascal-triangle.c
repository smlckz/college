#include <math.h>
#include <stdio.h>
#include <stdlib.h>

static int count_digits(double n)
{
	return floor(log10(n)) + 1;
}

static double factorial(double n)
{
	return tgamma(n + 1);	
}

static double comb(long n, long r)
{
	return factorial(n) / (factorial(r) * factorial(n - r));
}

int main(void)
{
	int i, j, k, h, l, *arr, *p;
	size_t n;
	printf("To Print the Pascal's Triangle\n\n");
	printf("Enter a height: ");
	scanf("%d", &h);
	l = count_digits(comb(h, h / 2));
	arr = malloc((h + 3) * sizeof(int));
	if (arr == NULL) {
		printf("Insufficient memory.\n");
		return 0;
	}
	arr[0] = 1;
	arr[1] = 0;
	n = 1;
	for (i = 1; i <= h + 1; i++) {
		for (k = 1; k <= (h - i + 1) * l; k++) {
			printf(" ");
		}
		for (k = 0; k < n; k++) {
			printf("%- *d ", l + 1, arr[k]);
		}
		printf("\n");
		arr[k+1] = 0;
		for (n++; k > 0; k--) {
			arr[k] += arr[k-1];
		}
	}
	free(arr);
	return 0;
}

