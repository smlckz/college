#include <stdio.h>

int main(void)
{
	int i, n;
	float sum;
	printf("Calculate the sum of 1/i from i to a given number.\n\n");
	printf("Enter a number: ");
	scanf("%d", &n);
	sum = 0;
	for (i = 1; i <= n; i++) {
		sum += 1/(float)i;
	}
	printf("The sum is %f\n", sum);
	return 0;
}

/*
Output:
Set 1:
Calculate the sum of 1/i from i to a given number.

Enter a number: 1
The sum is 1.000000

Set 2:
Calculate the sum of 1/i from i to a given number.

Enter a number: 2
The sum is 1.500000

Set 3:
Calculate the sum of 1/i from i to a given number.

Enter a number: 10
The sum is 2.928968

Set 4:
Calculate the sum of 1/i from i to a given number.

Enter a number: 100
The sum is 5.187378

*/

