#include <stdio.h>

int main(void)
{
	int i, sum, n, a;
	printf("Calculate the sum of (-1)^(n+1) * i for i from 1 to given number.\n\n");
	printf("Enter a number: ");
	scanf("%d", &n);
	a = 1;
	sum = 0;
	for (i = 1; i <= n; i++) {
		sum += a * i;
		a = -a;
	}
	printf("The sum is %d\n", sum);
	return 0;
}

/*
Output:
Set 1:
Calculate the sum of (-1)^(n+1) * i for i from 1 to given number.

Enter a number: 1
The sum is 1

Set 2:
Calculate the sum of (-1)^(n+1) * i for i from 1 to given number.

Enter a number: 2
The sum is -1

Set 3:
Calculate the sum of (-1)^(n+1) * i for i from 1 to given number.

Enter a number: 10
The sum is -5

Set 4:
Calculate the sum of (-1)^(n+1) * i for i from 1 to given number.

Enter a number: 123
The sum is 62

*/

