#include <stdio.h>

#define STEPS 100

double my_exp(double x)
{
	int i;
	double n = 1;
	double p = 1;
	for (i = 1; i <= STEPS; i++) {
		p *= x / i;
		n += p;
	}
	return n;
}

int main(void)
{
	double x;
	printf("To calculate exp(x) for a given number x\n\n");
	printf("Enter a number: ");
	scanf("%lf", &x);
	printf("exp(%.14g) = %.14g\n", x, my_exp(x));
	return 0;
}

/*
Output:
Set 1:
To calculate exp(x) for a given number x

Enter a number: 1
exp(1) = 2.718281828459

Set 2:
To calculate exp(x) for a given number x

Enter a number: 2
exp(2) = 7.3890560989306

Set 3:
To calculate exp(x) for a given number x

Enter a number: 0.1 
exp(0.1) = 1.1051709180756

Set 4:
To calculate exp(x) for a given number x

Enter a number: -1
exp(-1) = 0.36787944117144

*/

