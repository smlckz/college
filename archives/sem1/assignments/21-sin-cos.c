#include <stdio.h>

#define STEPS 1000

double my_sin(double x)
{
	int i;
	double n = x;
	double p = x;
	for (i = 2; i <= STEPS * 2; i += 2) {
		p *= -1 * x * x / (i * (i + 1));
		n += p;
	}
	return n;
}

double my_cos(double x)
{
	int i;
	double n = 1;
	double p = 1;
	for (i = 2; i <= STEPS * 2; i += 2) {
		p *= -1 * x * x / (i * (i - 1));
		n += p;
	}
	return n;
}

int main(void)
{
	double x;
	printf("To calculate the sine and cosine of a given number\n\n");
	printf("Enter a number: ");
	scanf("%lf", &x);
	printf("sin(%.14g) = %.14g\n", x, my_sin(x));
	printf("cos(%.14g) = %.14g\n", x, my_cos(x));
	return 0;
}

/*
Output:
Set 1:
To calculate the sine and cosine of a given number

Enter a number: 0
sin(0) = 0
cos(0) = 1

Set 2:
To calculate the sine and cosine of a given number

Enter a number: 1.5707963267948966
sin(1.5707963267949) = 1
cos(1.5707963267949) = 4.2647940513479e-17

Set 3:
To calculate the sine and cosine of a given number

Enter a number: 3.141592653579893
sin(3.1415926535799) = 9.9005553937036e-12
cos(3.1415926535799) = -1

Set 4:
To calculate the sine and cosine of a given number

Enter a number: -0.13720941543579
sin(-0.13720941543579) = -0.13677929342038
cos(-0.13720941543579) = 0.99060154698618

*/

