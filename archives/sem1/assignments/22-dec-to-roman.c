#include <stdio.h>

void roman_part(int k, char u, char h, char p)
{
	int i; 
	if (k == 9) {
		printf("%c%c", u, p);
	} else if (k >= 5) {
		printf("%c", h);
		for (i = 6; i <= k; i++) {
			printf("%c", u);
		}
	} else if (k == 4) {
		printf("%c%c", u, h);
	} else if (k > 0) {
		for (i = 1; i <= k; i++) {
			printf("%c", u);
		}
	}
}

void decimal_to_roman(int n)
{
	int i, k;
	if (n >= 1000) {
		k = n / 1000;
		for (i = 1; i <= k; i++) {
			printf("M");
		} 
	}
	roman_part((n / 100) % 10, 'C', 'D', 'M');
	roman_part((n / 10) % 10, 'X', 'L', 'C');
	roman_part(n % 10, 'I', 'V', 'X');
}

int main(void)
{
	int n;
	printf("To convert a decimal number to roman number\n\n");
	printf("Enter a number: ");
	scanf("%d", &n);
	if (n < 1 || n > 3999) {
		printf("Invalid input: it must be between 1 and 3999, inclusive.\n");
		return 0;
	}
	printf("The Roman numeral ");
	decimal_to_roman(n);
	printf(" corresponds to the Arabic numeral %d\n", n);
	return 0;
}

/*
Output:
Set 1:
To convert a decimal number to roman number

Enter a number: 0
Invalid input: it must be between 1 and 3999, inclusive.

Set 2:
To convert a decimal number to roman number

Enter a number: 123
The Roman numeral CXXIII corresponds to the Arabic numeral 123

Set 3:
To convert a decimal number to roman number

Enter a number: 987
The Roman numeral CMLXXXVII corresponds to the Arabic numeral 987

Set 4:
To convert a decimal number to roman number

Enter a number: 1234
The Roman numeral MCCXXXIV corresponds to the Arabic numeral 1234

Set 5:
To convert a decimal number to roman number

Enter a number: 2946
The Roman numeral MMCMXLVI corresponds to the Arabic numeral 2946

*/

