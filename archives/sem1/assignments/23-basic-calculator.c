#include <stdio.h>

void read_values(int *a, int *b)
{
	printf("Enter two values: ");
	scanf("%d%d", a, b);
}

int main(void)
{
	int a, b, c, choice;
	for (;;) {
		printf("\nEnter:\n1. Addition\n2. Subtraction\n3. Multiplication\n4. Division\n5. Exit\n\n");
		printf("Enter choice: ");
		scanf("%d", &choice);
		switch (choice) {
		case 1:
			read_values(&a, &b);
			printf("The sum of %d and %d is %d\n", a, b, a + b);
			break;
		case 2:
			read_values(&a, &b);
			printf("The difference of %d and %d is %d\n", a, b, a - b);
			break;
		case 3:
			read_values(&a, &b);
			printf("The product of %d and %d is %d\n", a, b, a * b);
			break;
		case 4:
			read_values(&a, &b);
			if (b == 0) {
				printf("Can not divide by zero.\n");
				return 0;
			}
			printf("The quotient of %d and %d is %d\n", a, b, a / b);
			break;
		case 5:
			printf("\nBye\n\n");
			return 0;
		default:
			printf("Invalid choice\nPlease choose a valid option given in the menu.\n");
		}
	}
	return 0;
}

/*
Output:
=== Set 1 ===

Enter:
1. Addition
2. Subtraction
3. Multiplication
4. Division
5. Exit

Enter choice: 1
Enter two values: 123 456
The sum of 123 and 456 is 579

Enter:
1. Addition
2. Subtraction
3. Multiplication
4. Division
5. Exit

Enter choice: 2   
Enter two values: 789 3456
The difference of 789 and 3456 is -2667

Enter:
1. Addition
2. Subtraction
3. Multiplication
4. Division
5. Exit

Enter choice: 3
Enter two values: 78 45
The product of 78 and 45 is 3510

Enter:
1. Addition
2. Subtraction
3. Multiplication
4. Division
5. Exit

Enter choice: 4   
Enter two values: 12 5
The quotient of 12 and 5 is 2

Enter:
1. Addition
2. Subtraction
3. Multiplication
4. Division
5. Exit

Enter choice: 5

Bye

=== Set 2 ===

Enter:
1. Addition
2. Subtraction
3. Multiplication
4. Division
5. Exit

Enter choice: 4
Enter two values: 1 0
Can not divide by zero.

=== Set 3 ===

Enter:
1. Addition
2. Subtraction
3. Multiplication
4. Division
5. Exit

Enter choice: 0
Invalid choice
Please choose a valid option given in the menu.

Enter:
1. Addition
2. Subtraction
3. Multiplication
4. Division
5. Exit

Enter choice: 5

Bye



*/

