#include <stdio.h>

void swap_by_value(int a, int b)
{
	int c = a;
	b = a;
	b = c;
}

void swap_by_ref(int *a, int *b)
{
	int c = *a;
	*a = *b;
	*b = c;
}

int main(void)
{
	int x, y;
	printf("Enter two numbers: ");
	scanf("%d%d", &x, &y);
	printf("The values are: %d %d\n", x, y);
	swap_by_value(x, y);
	printf("The values are: %d %d\n", x, y);
	swap_by_ref(&x, &y);
	printf("The values are: %d %d\n", x, y);
	return 0;
}

/*
Output:
Enter two numbers: 2 3
The values are: 2 3
The values are: 2 3
The values are: 3 2

*/

