#include <stdio.h>

int fact(int n)
{
	int i, f = 1;
	for (i = n; i > 1; i--) f *= i;
	return f;
}

int is_krishnamurti(int n)
{
	int m = n, sum = 0, r;
	while (m > 0) {
		r = m % 10;
		sum += fact(r);
		m /= 10;
	}
	return n == sum;
}

int main(void)
{
	int n;
	printf("To check whether a given number is a Krishnamurti number or not\n\n");
	printf("Enter a number: ");
	scanf("%d", &n);
	printf("%d is%s a Krishnamurti number.\n", n, is_krishnamurti(n) ? "" : " not");
	return 0;
}


/*
Output:
Set 1:
To check whether a given number is a Krishnamurti number or not

Enter a number: 2
2 is a Krishnamurti number.

Set 2:
To check whether a given number is a Krishnamurti number or not

Enter a number: 145
145 is a Krishnamurti number.

Set 3:
To check whether a given number is a Krishnamurti number or not

Enter a number: 123
123 is not a Krishnamurti number.

*/

