#include <stdio.h>
#include <math.h>
#include <stdbool.h>

bool is_prime(int n)
{
	int i, u = sqrt(n);
	for (i = 2; i <= u; i++) {
		if (n % i == 0) return false;
	}
	return true;
}

int main(void)
{
	int lb, ub, i;
	bool found = false;
	printf("To find twin primes in a given range\n\n");
	printf("Enter the lower bound of the range: ");
	scanf("%d", &lb);
	printf("Enter the upper bound of the range: ");
	scanf("%d", &lb);
	for (i = lb; i <= ub - 2; i++) {
		if (is_prime(i) && is_prime(i + 2)) {
			if (!found)
				printf("The twin primes between %d and %d are:\n", lb, ub);
			found = true;
			printf("%d %d\n", i, i + 2);
		}
	}
	if (!found) {
		printf("No twin primes were found in the given range.\n");
	}
	return 0;
}

