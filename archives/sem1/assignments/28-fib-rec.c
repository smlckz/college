#include <stdio.h>

int fib(int n)
{
	if (n == 1)
		return 0;
	else if (n == 2)
		return 1;
	else
		return fib(n - 1) + fib(n - 2);
}

int main(void)
{
	int i,n;
	printf("To calculate the Fibonacci Series upto a given number\n\n");
	printf("Enter a number: ");
	scanf("%d", &n);
	if (n <= 0) {
		printf("Invalid input, must be greater than 0\n");
		return 0;
	}
	printf("The first %d elements of the Fibonacci series: ", n);
	for (i = 1; i <= n; i++) {
		printf("%d ", fib(i));
	}
	printf("\n");
	return 0;
}

/*
Output:
Set 1:
To calculate the Fibonacci Series upto a given number

Enter a number: 0 
Invalid input, must be greater than 0

Set 2:
To calculate the Fibonacci Series upto a given number

Enter a number: 5
The first 5 elements of the Fibonacci series: 0 1 1 2 3 

Set 3:
To calculate the Fibonacci Series upto a given number

Enter a number: 10
The first 10 elements of the Fibonacci series: 0 1 1 2 3 5 8 13 21 34 

Set 4:
To calculate the Fibonacci Series upto a given number

Enter a number: 25
The first 25 elements of the Fibonacci series: 0 1 1 2 3 5 8 13 21 34 55 89 144 233 377 610 987 1597 2584 4181 6765 10946 17711 28657 46368 

*/

