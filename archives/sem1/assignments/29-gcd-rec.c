#include <stdio.h>

int gcd(int a, int b)
{
	if (b == 0)
		return a;
	else
		return gcd(b, a % b);
}

int main(void)
{
	int a, b;
	printf("To calculate the Greatest Common Divisor of given two numbers\n\n");
	printf("Enter two numbers: ");
	scanf("%d%d", &a, &b);
	printf("The GCD of %d and %d is %d\n", a, b, gcd(a, b));
	return 0;
}

/*
Output:
Set 1:
To calculate the Greatest Common Divisor of given two numbers

Enter two numbers: 2 3
The GCD of 2 and 3 is 1

Set 2:
To calculate the Greatest Common Divisor of given two numbers

Enter two numbers: 15 18
The GCD of 15 and 18 is 3

Set 3:
To calculate the Greatest Common Divisor of given two numbers

Enter two numbers: 16 12
The GCD of 16 and 12 is 4

Set 4:
To calculate the Greatest Common Divisor of given two numbers

Enter two numbers: 65 91
The GCD of 65 and 91 is 13


*/

