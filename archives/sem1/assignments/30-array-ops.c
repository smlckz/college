/* Array operations in a menu-driven program */

#include <stdio.h>
#include <stdlib.h>

int read_array(int **arr)
{
	int i, n;
	printf("Enter the number of elements: ");
	scanf("%d", &n);
	*arr = realloc(*arr, n * sizeof(int));
	if (*arr == NULL) {
		printf("Could not allocate array, try again\n");
		return 0;
	}
	printf("Enter the elements of array: ");
	for (i = 0; i < n; i++) {
		scanf("%d", *arr + i);
	}
	return n;
}

void print_array(char *prompt, int *arr, int n)
{
	int i;
	if (prompt == NULL) {
		printf("The elements of the array:");
	} else {
		printf("%s, the elements of the array:", prompt);
	}
	if (n == 0) {
		printf(" (empty array)");
	}
	for (i = 0; i < n; i++) {
		printf(" %d", arr[i]);
	}
	printf("\n");
}

int array_index_of(int *arr, int n, int item)
{
	int i;
	for (i = 0; i < n; i++) {
		if (arr[i] == item) return i;
	}
	return -1;
}

void find_in_array(int *arr, int n)
{
	int item, pos;
	print_array(NULL, arr, n);
	printf("Enter the element to be searched: ");
	scanf("%d", &item);
	pos = array_index_of(arr, n, item);
	if (pos < 0)
		printf("The element was not found in array\n");
	else
		printf("The element was found at %d\n", pos);
}

#define SWAP(A, B) (A) ^= (B), (B) ^= (A), (A) ^= (B)

void reverse_array(int *arr, int n)
{
	int i, j;
	for (i = 0, j = n - 1; i < j; i++, j--)
		SWAP(arr[i], arr[j]);
}

void sort_array(int *arr, int n)
{
	/* Bubble sort, *no* optimizations */
	int i, j;
	for (i = 0; i < n; i++)
		for (j = 0; j < n - 1; j++)
			if (arr[j] > arr[j + 1])
				SWAP(arr[j], arr[j + 1]);
}

void min_max_elem_of_array(int *arr, int n)
{
	int i, minval_idx = 0, maxval_idx = 0;
	for (i = 1; i < n; i++) {
		if (arr[minval_idx] > arr[i])
			minval_idx = i;
		if (arr[maxval_idx] < arr[i])
			maxval_idx = i;
	}
	if (n > 0) {
		printf("The minimum element is %d\n", arr[minval_idx]);
		printf("The maximum element is %d\n", arr[maxval_idx]);
	} else {
		printf("No minimum or maximum element in empty array\n");
	}
}

void sum_average_of_array(int *arr, int n)
{
	int i, sum;
	for (i = 0, sum = 0; i < n; i++) {
		sum += arr[i];
	}
	printf("Sum of elements: %d\n", sum);
	printf("Average: %f\n", (float)sum / n);
}

void find_and_replace_in_array(int *arr, int n)
{
	int i, elem, pos, repl;
	print_array(NULL, arr, n);
	printf("Enter the element to be replaced: ");
	scanf("%d", &elem);
	pos = array_index_of(arr, n, elem);
	if (pos < 0) {
		printf("Could not find the element to be replaced in array\n");
		return;
	}
	printf("Enter the replacement value: ");
	scanf("%d", &repl);
	arr[pos] = repl;
	print_array("After replacement", arr, n);
}

void copy_array(int *dst, int *src, int n)
{
	int i;
	for (i = 0; i < n; i++) {
		dst[i] = src[i];
	}
}

void count_frequency_of_array_elements(int *arr, int n)
{
	int *aarr = malloc(n * sizeof(int));
	int i, j, freq;
	if (aarr == NULL) {
		printf("Could not allocate space, try again\n");
		return;
	}
	copy_array(aarr, arr, n);
	sort_array(aarr, n);
	printf("The frequency of the elements in the given array:\n");
	if (n > 0) {
		printf("Element => Frequency\n");
	} else {
		printf("(empty array)");
	}
	for (i = 0; i < n; i++) {
		for (j = i, freq = 1; j < n - 1 && aarr[j] == aarr[j + 1]; j++, freq++);
		printf("%d => %d\n", aarr[j], freq);
		if (freq > 1) {
			i = j;
		}
	}
	free(aarr);
}

int remove_duplicates(int **arr, int n)
{
	int *aarr = malloc(n * sizeof(int)), *tmp;
	int i, j, nn, found;
	if (aarr == NULL) {
		printf("Could not allocate space, try again\n");
		return n;
	}
	for (i = 0, nn = 0; i < n; i++) {
		for (j = 0, found = 0; j < nn; j++) {
			if ((*arr)[i] == aarr[j]) {
				found = 1;
				break;
			}
		}
		if (!found) {
			aarr[nn++] = (*arr)[i];
		}
	}
	copy_array(*arr, aarr, nn);
	if (nn < n && (tmp = realloc(*arr, nn * sizeof(int))) == NULL) {
		printf("Could not resize array after removing duplicates.\n");
		*arr = tmp;
	}
	return nn;
}

int main(void)
{
	int choice, n, *arr;
	arr = NULL;
	n = 0;
	for (;;) {
		printf("Menu:\n"
		       " 1  Input elements into array\n"
		       " 2  Print elements of array\n"
		       " 3  Search for an element in array\n"
		       " 4  Reverse array\n"
		       " 5  Sort array\n"
		       " 6  Find the minimum and maximum element of array\n"
		       " 7  Sum and average of the elements of array\n"
		       " 8  Find and replace element in array\n"
		       " 9  Count the frequency of elements in array\n"
		       "10  Remove duplicate elements in array\n"
		       " 0  Exit\n\n");
		printf("Enter your choice: ");
		scanf("%d", &choice);
		switch (choice) {
		case 0:
			free(arr);
			printf("Bye\n");
			exit(0);
		case 1:
			n = read_array(&arr);
			break;
		case 2:
			print_array(NULL, arr, n);
			break;
		case 3:
			find_in_array(arr, n);
			break;
		case 4:
			print_array("Before reversal", arr, n);
			reverse_array(arr, n);
			print_array("After reversal", arr, n);
			break;
		case 5:
			print_array("Before sorting", arr, n);
			sort_array(arr, n);
			print_array("After sorting", arr, n);
			break;
		case 6:
			print_array(NULL, arr, n);
			min_max_elem_of_array(arr, n);
			break;
		case 7:
			sum_average_of_array(arr, n);
			break;
		case 8:
			find_and_replace_in_array(arr, n);
			break;
		case 9:
			count_frequency_of_array_elements(arr, n);
			break;
		case 10:
			print_array("Before removing duplicates", arr, n);
			n = remove_duplicates(&arr, n);
			print_array("After removing duplicates", arr, n);
			break;
		default:
			printf("Invalid choice, try again\n");
		}
		printf("\n");
	}
}

