/* Array operations in a menu-driven program */

#include <stdio.h>
#include <stdlib.h>

int read_array(int **arr)
{
	int i, n;
	printf("Enter the number of elements: ");
	scanf("%d", &n);
	*arr = malloc(n * sizeof(int));
	if (*arr == NULL) {
		printf("Could not allocate array, try again\n");
		return 0;
	}
	printf("Enter the elements of array: ");
	for (i = 0; i < n; i++) {
		scanf("%d", *arr + i);
	}
	return n;
}

void print_array(int *arr, int n)
{
	int i;
	if (n == 0) {
		printf(" (empty array)");
	}
	for (i = 0; i < n; i++) {
		printf(" %d", arr[i]);
	}
	printf("\n");
}

#define SWAP(A, B) (A) ^= (B), (B) ^= (A), (A) ^= (B)

void sort_array(int *arr, int n)
{
	/* Bubble sort, *no* optimizations */
	int i, j;
	for (i = 0; i < n; i++)
		for (j = 0; j < n - 1; j++)
			if (arr[j] > arr[j + 1])
				SWAP(arr[j], arr[j + 1]);
}

void merge(int *a1, int a1sz, int *a2, int a2sz, int *a)
{
	int i, j, k;
	for (i = 0, j = 0, k = 0; k < (a1sz + a2sz); k++) {
		if (i == a1sz) a[k] = a2[j++];
		else if (j == a2sz) a[k] = a1[i++];
		else if (a1[i] <= a2[j]) a[k] = a1[i++];
		else a[k] = a2[j++];
	}
}

int main(void)
{
	int *a1, *a2, *a;
	int a1sz, a2sz;
	puts("To merge two given sorted array\n");
	puts("Enter the first array:");
	a1sz = read_array(&a1);
	if (a1sz < 0) {
		puts("Unable to read first array");
		return 1;
	}
	sort_array(a1, a1sz);
	puts("Enter the second array:");
	a2sz = read_array(&a2);
	if (a2sz < 0) {
		puts("Unable to read second array");
		return 2;
	}
	sort_array(a2, a2sz);
	printf("The first sorted array: ");
	print_array(a1, a1sz);
	printf("The second sorted array: ");
	print_array(a2, a2sz);
	a = malloc((a1sz + a2sz) * sizeof(int));
	if (a == NULL) {
		puts("Unable to make merged array");
		return 3;
	}
	merge(a1, a1sz, a2, a2sz, a);
	printf("The merged array is: ");
	print_array(a, a1sz + a2sz);
	free(a1); free(a2); free(a);
	return 0;
}

