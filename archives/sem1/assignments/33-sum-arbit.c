#include <stdio.h>
#include <stdlib.h>

int main(void) {
	int n, i, sum, *a;
	printf("To calculate the sum of given integers\n\n");
	printf("Enter the number of integers you want to enter: ");
	scanf("%d", &n);
	if (n <= 0) {
		printf("Invalid number of integers, must be positive\n");
		return 2;
	}
	a = malloc(n * sizeof(int));
	if (a == NULL) {
		printf("Not enough memory available\n");
		return 1;
	}
	printf("Enter %i integers: ", n);
	for (i = 0; i < n; i++) {
		scanf("%d", a + i);
	}
	for (i = 0, sum = 0; i < n; i++) {
		sum += a[i];
	}
	printf("The sum of the given elements is %i\n", sum);
	free(a);
	return 0;
}

