#include <stdio.h>
#include <stdlib.h>

void reverse(int *a, size_t n)
{
	size_t i, j = n - 1;
	int t;
	for (i = 0; i < n/2; i++, j--)
		t = a[i], a[i] = a[j], a[j] = t;
}

int main(void)
{
	int *a, n, i;
	printf("Enter the number of integers you want to enter: ");
	scanf("%d", &n);
	if (n <= 0) {
		printf("Invalid number of integers, must be positive\n");
		return 2;
	}
	a = malloc(n * sizeof(int));
	if (a == NULL) {
		printf("Not enough memory available\n");
		return 1;
	}
	printf("Enter %i integers: ", n);
	for (i = 0; i < n; i++) {
		scanf("%d", a + i);
	}
	printf("The array elements are: ");
	for (i = 0; i < n; i++) {
		printf("%i ", a[i]);
	}
	printf("\n");
	reverse(a, n);
	printf("The array elements after reverse are: ");
	for (i = 0; i < n; i++) {
		printf("%i ", a[i]);
	}
	printf("\n");
	return 0;
}

