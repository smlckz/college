#include <stdio.h>

void sort(int *a, int n)
{
	/* Bubble sort */
	int i, j, tmp;
	for (i = 0; i < n; i++) {
		for (j = 1; j < n - i; j++) {
			if (a[j] < a[j-1]) {
				tmp = a[j];
				a[j] = a[j-1];
				a[j-1] = tmp;
			}
		}
	}
}

int main(void)
{
	int a[10], *p = a, *e = a+10;
	printf("Enter 10 integers elements of array: ");
	while (p < e) {
		scanf("%i", p++);
	}
	sort(a, 10);
	printf("The array elements in ascending order: ");
	for (p = a; p < e; p++) {
		printf("%i ", *p);
	}
	printf("\n");
	printf("The array elements in descending order: ");
	for (p = a; p < e--;) {
		printf("%i ", *e);
	}
	printf("\n");
}

