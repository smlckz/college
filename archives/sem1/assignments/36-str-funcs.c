#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LINESIZ 1024

void read_line(char *s, size_t cap)
{
	//fflush(stdin);
	if (!fgets(s, cap, stdin)) return;
	size_t len = strlen(s);
	if (s[len - 1] == '\n') s[len - 1] = '\0';
}

void show_addresses(char *s)
{
	puts("The addresses of the characters in the given string: ");
	for (; *s != '\0'; s++) {
		printf("%p: %c\n", s, *s);
	}
}

int tolowercase(int c)
{
	return (c >= 'A' && c <= 'Z') ? c - 'A' + 'a' : c;
}

int touppercase(int c)
{
	return (c >= 'a' && c <= 'z') ? c - 'a' + 'A' : c;
}

void strtolowercase(char *s)
{
	for (; *s != '\0'; s++)
		*s = tolowercase(*s);
}

void strtouppercase(char *s)
{
	for (; *s != '\0'; s++)
		*s = touppercase(*s);
}

int xstrlen(char *s)
{
	char *p = s;
	while (*p++ != '\0');
	return --p - s;
}

int main(void)
{
	int choice, cmp;
	char line[LINESIZ], s1[2*LINESIZ], s2[LINESIZ];
	for (;;) {
		printf("\nMenu:\n"
		       "1  Show address of each character in string\n"
		       "2  Concatenate two strings\n"
		       "3  Comapre two strings\n"
		       "4  Calculate the length of the string\n"
		       "5  Convert all lowercase characters to uppercase\n"
		       "6  Convert all uppercase characters to lowercase\n"
		       "0  Exit\n\n");
		printf("Enter your choice: ");
		scanf("%d%*c", &choice);
		switch (choice) {
		case 0:
			puts("Bye");
			exit(0);
		case 1:
			printf("Enter a string: ");
			read_line(line, LINESIZ);
			show_addresses(line);
			break;
		case 2:
			printf("Enter first string: ");
			read_line(s1, LINESIZ);
			printf("Enter second string: ");
			read_line(s2, LINESIZ);
			strcat(s1, s2);
			puts("The concatenated string is:");
			puts(s1);
			break;
		case 3:
			printf("Enter first string: ");
			read_line(s1, LINESIZ);
			printf("Enter second string: ");
			read_line(s2, LINESIZ);
			cmp = strcmp(s1, s2);
			if (cmp == 0) {
				printf("The given strings are equal.\n");
			} else if (cmp < 0) {
				printf("The first string lexicographically appears before the second string\n");
			} else {
				printf("The second string lexicographically appears before the first string\n");   
			}
			break;
		case 4:
			printf("Enter a string: ");
			read_line(line, LINESIZ);
			printf("The given string is %i character long\n", xstrlen(line));
			break;
		case 5:
			printf("Enter a string: ");
			read_line(line, LINESIZ);
			strtouppercase(line);
			puts("After conversion from lowercase to uppercase:");
			puts(line);
			break;
		case 6:
			printf("Enter a string: ");
			read_line(line, LINESIZ);
			strtolowercase(line);
			puts("After conversion from uppercase to lowercase:");
			puts(line);
			break;
		}
	}
	
}

