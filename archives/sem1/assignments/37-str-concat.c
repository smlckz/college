#include <stdio.h>
#include <string.h>

#define LINESZ 100

void str_concat(char *dst, char *src)
{
	while (*dst++ != '\0');
	dst--;
	while ((*dst++ = *src++) != '\0');
}

void read_line(char *s, size_t cap)
{
	//fflush(stdin);
	if (!fgets(s, cap, stdin)) return;
	size_t len = strlen(s);
	if (s[len - 1] == '\n') s[len - 1] = '\0';
}

int main(void)
{
	char s1[2*LINESZ], s2[LINESZ];
	puts("Enter first string: ");
	read_line(s1, LINESZ);
	puts("Enter second string: ");
	read_line(s2, LINESZ);
	str_concat(s1, s2);
	puts("The concatenation of the given strings is: ");
	puts(s1);
	return 0;
}

