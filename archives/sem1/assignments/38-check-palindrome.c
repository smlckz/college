#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define LINESZ 100

void read_line(char *s, size_t cap)
{
	//fflush(stdin);
	if (!fgets(s, cap, stdin)) return;
	size_t len = strlen(s);
	if (s[len - 1] == '\n') s[len - 1] = '\0';
}

bool is_palindrome(char *s)
{
	int len = strlen(s);
	char *p = s + len - 1;
	for (; s < p; s++, p--) {
		if (*s != *p) return false;
	}
	return true;
}

int main(void)
{
	char line[LINESZ];
	printf("Enter a string: ");
	read_line(line, LINESZ);
	printf("The given string is%s a palindrome.\n",
	       is_palindrome(line) ? "" : " not");
}

