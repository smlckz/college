#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define LINESZ 100

bool is_alpha(char c)
{
	return ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z');
}

bool is_vowel(char c)
{
	return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u'
		|| c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U';
}


void read_line(char *s, size_t cap)
{
	//fflush(stdin);
	if (!fgets(s, cap, stdin)) return;
	size_t len = strlen(s);
	if (s[len - 1] == '\n') s[len - 1] = '\0';
}

int main(void)
{
	char line[LINESZ], *p = line;
	int vowels = 0, consonants = 0, spaces = 0;
	printf("Enter a line: ");
	read_line(line, LINESZ);
	for (; *p != '\0'; p++) {
		if (is_alpha(*p)) {
			if (is_vowel(*p)) vowels++;
			else consonants++;
		} else if (*p == ' ') {
			spaces++;
		}
	}
	printf("The given sentence has %i vowels, %i consonants and %i spaces.\n", vowels, consonants, spaces);
	return 0;
}

