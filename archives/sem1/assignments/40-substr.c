#include <stdio.h>
#include <string.h>

#define LINESZ 100

void read_line(char *s, size_t len)
{
	size_t l;
	if (!fgets(s, len, stdin)) return;
	l = strlen(s);
	if (s[l - 1] == '\n') s[l - 1] = '\0';
}

int is_substr(char *h, char *n)
{
	char *m, *p;
	int eq;
	for (; *h != '\0'; h++) {
		for (m = n, p = h, eq = 1; *m != '\0'; m++, h++) {
			if (*m != *p) {
				eq = 0;
				break;
			}
		} 
		if (eq) return 1;
	}
	return 0;
}

int main(void)
{
	char h[LINESZ], n[LINESZ];
	printf("Enter a string: ");
	read_line(h, LINESZ);
	printf("Enter a substring to search in the given string: ");
	read_line(n, LINESZ);
	printf("`%s` does%s have `%s` as a substring.\n", h,
	       is_substr(h, n) ? "" : " not", n);
	return 0;
}

