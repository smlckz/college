#include <stdio.h>
#include <string.h>

int main(int argc, char **argv)
{
	char t[256], *p;
	size_t i;
	memset(t, 0, sizeof t);
	while ((p = *++argv) != NULL) {
		while (*p != '\0')
			t[*p++]++;
	}
	printf("Table: Frequency of alphabetic characters in command line arguments\n");
	printf("+-----------+-----------+\n"
	       "| Character | Frequency |\n"
	       "+-----------+-----------+\n");
	for (i = 'a'; i <= 'z'; i++) {
		if (t[i] != 0 || t[i-'a'+'A'] != 0)
			printf("|    `%c`    | %9d |\n", i, t[i] + t[i-'a'+'A']);
	}
	printf("+-----------+-----------+\n\n");
	return 0;
}

