#include <stdio.h>
#include <string.h>

#define LINESZ 100

void read_line(char *s, size_t n)
{
	size_t l;
	if (!fgets(s, n, stdin)) return;
	l = strlen(s);
	if (s[l - 1] == '\n') s[l - 1] = '\0';
}

int main(void)
{
	char line[LINESZ], filename[LINESZ];
	FILE *f;
	int c;
	printf("To read lines from user to save into a file\n\n");
	printf("Enter file name: ");
	read_line(filename, LINESZ);
	f = fopen(filename, "w+");
	if (f == NULL) {
		printf("Could not open file `%s` for writing\n", filename);
		return 1;
	}
	printf("Enter text to be written into the file:\n");
	while (1) {
		read_line(line, LINESZ);
		if (strlen(line) == 0) break;
		fprintf(f, "%s\n", line);
	}
	fflush(f);
	rewind(f);
	printf("Contents of the file thus entered is:\n");
	while ((c = fgetc(f)) != EOF) {
		putchar(c);
	}
	fclose(f);
	return 0;
}

