#include <stdio.h>
#include <string.h>

#define LINESZ 100

void read_line(char *s, size_t len)
{
	size_t l;
	if (!fgets(s, len, stdin)) return;
	l = strlen(s);
	if (s[l - 1] == '\n') s[l - 1] = '\0';
}

int toloweralpha(int c)
{
	if ('a' <= c && c <= 'z')
		return c;
	else if ('A' <= c && c <= 'Z')
		return c-'A'+'a';
	return -1;
}

int isvowel(int c)
{
	return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u';
}

int main(void)
{
	char filename[LINESZ];
	FILE *f;
	int c, a;
	int ndigits = 0, nchrs = 0, nspecials = 0, ncons = 0, nvowels = 0;
	printf("Count number of characters, digits, blanks in a file\n\n");
	printf("Enter a file name: ");
	read_line(filename, LINESZ);
	f = fopen(filename, "r");
	if (f == NULL) {
		printf("Could not open file `%s` for reading\n", filename);
		return 1;
	}
	while ((c = fgetc(f)) != EOF) {
		if ('0' <= c && c <= '9') ndigits++;
		else if ((a = toloweralpha(c)) != -1) {
			if (isvowel(a)) nvowels++;
			else ncons++;
		} else nspecials++;
		nchrs++;
	}
	printf("The file `%s` has\n", filename);
	printf("  %d characters\n", nchrs);
	printf("  %d vowels\n", nvowels);
	printf("  %d consonants\n", ncons);
	printf("  %d digits and\n", ndigits);
	printf("  %d special characters\n", nspecials);
	return 0;
}

