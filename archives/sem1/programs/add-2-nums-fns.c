#include <stdio.h>

int add(int a, int b)
{
	return a + b;
}

int main(void)
{
	int a, b, sum;
	printf("Enter first number: ");
	scanf("%d", &a);
	printf("Enter second number: ");
	scanf("%d", &b);
	sum = add(a, b);
	printf("The sum of %d and %d is %d.\n", a, b, sum);
	return 0;
}

/*
Output:

Set 1:
Enter first number: 10
Enter second number: 20
The sum of 10 and 20 is 30.

Set 2:
Enter first number: 1 
Enter second number: 2
The sum of 1 and 2 is 3.

Set 3:
Enter first number: 33
Enter second number: 44
The sum of 33 and 44 is 77.

*/

