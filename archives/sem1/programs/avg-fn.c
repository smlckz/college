#include <stdio.h>

float average(float a, float b, float c)
{
	return (a + b + c) / 3;
}

int main(void)
{
	float a, b, c;
	printf("Average of three numbers\n\n");
	printf("Enter three numbers: ");
	scanf("%f%f%f", &a, &b, &c);
	printf("The average is %f\n", average(a, b, c));
	return 0;
}

/*
Output:
Set 1:
Average of three numbers

Enter three numbers: 1 2 3
The average is 2.000000

Set 2:
Average of three numbers

Enter three numbers: 1.5 2.3 7.9
The average is 3.900000

Set 3:
Average of three numbers

Enter three numbers: 123 45.6 7.89
The average is 58.829999

*/

