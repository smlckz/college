#include <stdio.h>
#include <string.h>

#define LINESZ 100

void read_line(char *s, size_t len)
{
	size_t l;
	if (!fgets(s, len, stdin)) return;
	l = strlen(s);
	if (s[l - 1] == '\n') s[l - 1] = '\0';
}

int main(void)
{
	char src[LINESZ], dst[LINESZ];
	FILE *srcf, *dstf;
	int c;
	printf("To copy a file to another file\n\n");
	printf("Enter the source file name: ");
	read_line(src, LINESZ);
	srcf = fopen(src, "r");
	if (srcf == NULL) {
		printf("Could not open source file `%s` for reading\n", src);
		return 1;
	}
	printf("Enter the destination file name: ");
	read_line(dst, LINESZ);
	dstf = fopen(dst, "w");
	if (srcf == NULL) {
		printf("Could not open source file `%s` for writing\n", src);
		return 1;
	}
	while ((c = fgetc(srcf)) != EOF)
		fputc(c, dstf);
	fclose(srcf);
	fclose(dstf);
	puts("File copied successfully");
	return 0;
}

