#include <stdio.h>
#include <string.h>

#define LINESZ 100

void read_line(char *s, size_t len)
{
	size_t l;
	if (!fgets(s, len, stdin)) return;
	l = strlen(s);
	if (s[l - 1] == '\n') s[l - 1] = '\0';
}

int main(void)
{
	char filename[LINESZ];
	FILE *f;
	int c;
	int nod = 0, noc = 0, nob = 0;
	printf("Count number of characters, digits, blanks in a file\n\n");
	printf("Enter a file name: ");
	read_line(filename, LINESZ);
	f = fopen(filename, "r");
	if (f == NULL) {
		printf("Could not open file `%s` for reading\n", filename);
		return 1;
	}
	while ((c = fgetc(f)) != EOF) {
		if ('0' <= c && c <= '9') nod++;
		if (c == ' ') nob++;
		noc++;
	}
	printf("The file `%s` has\n", filename);
	printf("%d characters, %d digits and %d spaces\n", noc, nod, nob);
	return 0;
}

