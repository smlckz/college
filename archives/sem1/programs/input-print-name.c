#include <stdio.h>

void xputs(char *str) {
	while (*str) putchar(*str++);
	putchar('\n');
}

size_t xstrlen(char *s)
{
	char *p = s;
	while (*p++ != '\0');
	return p - s - 1;
}

void xstrcat(char *d, char *s)
{
	while (*d++ != '\0');
	d--;
	while ((*d++ = *s++) != '\0');
}

int main(void) {
	char fname[50], lname[25];
	xputs("Enter first name");
	gets(fname);
	xputs("Enter last name");
	gets(lname);
	xputs("Your name");
	xstrcat(fname, lname);
	xputs(fname);
	printf("length: %zu\n", xstrlen(fname));
}


