#include <stdio.h>
#include <stdbool.h>

bool is_leap_year(int y)
{
	return y % 400 == 0 || (y % 100 != 0 && y % 4 == 0);
}

int main(void)
{
	int year;
	printf("To check whether a given year is leap year or not\n\n");
	printf("Enter a year: ");
	scanf("%d", &year);
	if (year <= 1200 || year >= 9999) {
		printf("Invalid year.\n");
		return 0;
	}
	printf("%d is%s a leap year.\n", year, is_leap_year(year) ? "" : " not");
	return 0;
}

/*
Output:
Set 1:


Set 2:

Set 3:

Set 4:

*/

