#include <stdio.h>

int main(void)
{
	FILE *f;
	int c;
	f = fopen("a.txt", "r");
	if (f == NULL) {
		puts("Could not open the file");
		return 1;
	}
	while ((c = fgetc(f)) != EOF) putchar(c);
	fclose(f);
	return 0;
}

