#include <assert.h>

int xstrlen(char *s)
{
        char *p = s;
        while (*p++);
        return --p - s;
}

int xstrcmp(char *s1, char *s2)
{
        for (; *s2 && *s1 == *s2; s1++, s2++);
        return *s1 - *s2;
}

int xstrcmpx(char *s1, char *s2)
{
        for (; *s1 && *s1 == *s2; s1++, s2++);
        return (*s1 - *s2) ? (*s1 < *s2) ? -1 : 1 : 0;
}

void xstrcpy(char *dst, char *src)
{
        while (*dst++ = *src++);
}

void xstrcat(char *dst, char *src)
{
        while (*dst++);
        dst--;
        while (*dst++ = *src++);
}

void xstrrev(char *s)
{
        char *r = s, t;
        while (*r++);
        if (r - s < 3) return;
        r -= 2;
        while (s < r) t = *s, *s++ = *r, *r-- = t;
}

int xstrlenr(char *str)
{
	int i = 0;
	while (str[i] != '\0')
		i++;
	return i;
}

void xstrrevr(char *str)
{
	int left, right, temp;
	int len = xstrlenr(str);
	if (len < 2) return;
	left = 0;
	right = len - 1;
	while (left < right) {
		temp = str[right];
		str[right] = str[left];
		str[left] = temp;
		left++;
		right--;
	}
}

int xstrcmpr(char *s1, char *s2)
{
	int i, diff;
	i = 0;
        do {
		diff = s1[i] - s2[i];
		if (s1[i] == '\0' || s2[i] == '\0') break;
                i++;
	} while (diff == 0);
	return diff;
}

void xstrcpyr(char *dst, char *src)
{
        int i;
        for (i = 0; src[i] != '\0'; i++)
                dst[i] = src[i];
        dst[i] = '\0';
}

int main(void)
{
	assert(xstrlen("") == 0);
	assert(xstrlen(".") == 1);
	assert(xstrlen("abcde") == 5);

        assert(xstrlenr("") == 0);
	assert(xstrlenr(".") == 1);
	assert(xstrlenr("abcde") == 5);
        
	assert(xstrcmp("", "") == 0);
        assert(xstrcmp("while", "whole") < 0);
        assert(xstrcmp("what", "wait") > 0);
        assert(xstrcmp("free", "freedom") < 0);
        assert(xstrcmp("technology", "tech") > 0);
        
        assert(xstrcmpr("", "") == 0);
        assert(xstrcmpr("while", "whole") < 0);
        assert(xstrcmpr("what", "wait") > 0);
        assert(xstrcmpr("free", "freedom") < 0);
        assert(xstrcmpr("technology", "tech") > 0);

        assert(xstrcmpx("", "") == 0);
        assert(xstrcmpx("while", "whole") == -1);
        assert(xstrcmpx("what", "wait") == 1);
        assert(xstrcmpx("free", "freedom") == -1);
        assert(xstrcmpx("technology", "tech") == 1);

        {
                char s1[] = "", s1p[sizeof(s1)];
                xstrcpy(s1p, s1);
                assert(!xstrcmp(s1, s1p));
                char s2[] = "a", s2p[sizeof(s2)];
                xstrcpy(s2p, s2);
                assert(!xstrcmp(s2, s2p));
                char s3[] = "abc", s3p[sizeof(s3)];
                xstrcpy(s3p, s3);
                assert(!xstrcmp(s3, s3p));
        }
        
        {
                char s1[] = "", s1p[sizeof(s1)];
                xstrcpyr(s1p, s1);
                assert(!xstrcmp(s1, s1p));
                char s2[] = "a", s2p[sizeof(s2)];
                xstrcpyr(s2p, s2);
                assert(!xstrcmp(s2, s2p));
                char s3[] = "abc", s3p[sizeof(s3)];
                xstrcpyr(s3p, s3);
                assert(!xstrcmp(s3, s3p));
        }

        {
                char s1[] = "";
                xstrrev(s1);
                assert(!xstrcmp(s1, s1));
                char s2[] = "a";
                xstrrev(s2);
                assert(!xstrcmp(s2, s2));
                char s3[] = "abc", s3p[sizeof(s3)];
                xstrcpy(s3p, s3);
                xstrrev(s3p);
                assert(!xstrcmp(s3p, "cba"));
                xstrrev(s3p);
                assert(!xstrcmp(s3p, s3));
        }

        {
                char s1[] = "";
                xstrrevr(s1);
                assert(!xstrcmp(s1, s1));
                char s2[] = "a";
                xstrrevr(s2);
                assert(!xstrcmp(s2, s2));
                char s3[] = "abc", s3p[sizeof(s3)];
                xstrcpy(s3p, s3);
                xstrrevr(s3p);
                assert(!xstrcmp(s3p, "cba"));
                xstrrevr(s3p);
                assert(!xstrcmp(s3p, s3));
        }
        
}

