#include <stdio.h>

#define LEN 100

size_t my_strlen(char *s)
{
	char *p = s;
	while (*p++ != '\0');
	return p - s - 1;
}



void remove_spaces(char *dst, char *src)
{
	while (*src != '\0') {
		if (*src == ' ') {
			src++;
		} else {
			*dst++ = *src++;
		}
	}
	*dst = '\0';
}

int main(void)
{
	char str[LEN], strpd[LEN];
	size_t len;
	printf("Enter a string: ");
	if (fgets(str, LEN, stdin) == NULL) {
		printf("Error taking input\n");
		return 1;
	}
	len = my_strlen(str);
	printf("Length: %zu\n", len);
	if (str[len-1] == '\n') str[len-1] = '\0';
	printf("The string is: %s\n", str);
	remove_spaces(strpd, str);
	printf("The string with spaces removed: %s\n", strpd);
	return 0;
}

