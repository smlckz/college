#include <stdio.h>
#include <stdlib.h>

#define SKIPNL "%*c"

struct student {
	char name[30];
	int age;
	int height;
};

int main(void)
{
	struct student *s;
	int i, n;
	int ages = 0, heights = 0;
	int min_age_idx = 0, max_height_idx = 0;
	printf("Enter the number of students: ");
	scanf("%d" SKIPNL, &n);
	s = malloc(n * sizeof(struct student));
	if (s == NULL) {
		printf("Could not allocate space.\n");
		return 1;
	}
	for (i = 0; i < n; i++) {
		printf("\nEnter the data of student %i:\n", i+1);
		printf("Name: ");
		gets(s[i].name);
		printf("Age: ");
		scanf("%i%*c", &s[i].age);
		printf("Height: ");
		scanf("%i%*c", &s[i].height);
	}
	puts("\nData that was entered\n");
	for (i = 0; i < n; i++) {
		printf("\nThe data of student %i:\n", i+1);
		printf("Name: %s\nAge: %i\nHeight: %i\n",
		       s[i].name, s[i].age, s[i].height);
	}
	for (i = 0; i < n; i++) {
		ages += s[i].age;
		heights += s[i].height;
	}
	printf("The average age of the students is %.1f\n", (float)ages/n);
	printf("The average height of the students is %.1f\n", (float)heights/n);
	for (i = 1; i < n; i++) {
		if (s[i].age < s[min_age_idx].age)
			min_age_idx = i;
		if (s[i].height > s[max_height_idx].height)
			max_height_idx = i;
	}
	printf("The youngest student is %s\n", s[min_age_idx].name);
	printf("The tallest student is %s\n", s[max_height_idx].name);
	return 0;
}


