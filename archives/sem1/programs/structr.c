#include <stdio.h>

struct student {
	char name[20];
	int roll;
	int age;
};

int main(void)
{
	struct student s;
	puts("Enter the data of the student");
	printf("Name: ");
	gets(s.name);
	printf("Roll number: ");
	scanf("%i", &s.roll);
	printf("Age: ");
	scanf("%i", &s.age);
	puts("\nThe data of the student: ");
	printf("Name: %s\nRoll No.: %i\nAge: %i\n", s.name, s.roll, s.age);
	return 0;
}

/*
Enter the data of the student
Name: Sudipto Mallick
Roll number: 571
Age: 18

The data of the student: 
Name: Sudipto Mallick
Roll No.: 571
Age: 18
*/

