#include <stdio.h>
#include <string.h>

int main(void)
{
	FILE *f;
	char buf[BUFSIZ];
	int c;
	size_t len;
	f = fopen("a.txt", "w");
	if (f == NULL) {
		puts("Could not open the file");
		return 1;
	}
	while (fgets(buf, BUFSIZ, stdin) != NULL)
		fprintf(f, "%s", buf);
	fclose(f);
	return 0;
}

