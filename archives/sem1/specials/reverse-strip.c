#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	FILE *sf, *df;
	char *s;
	int c;
	long i, j, slen, dlen;
	if (argc < 3) {
		printf("Usage: %s [source-file] [destination-file]\n", argv[0]);
		printf("Read from the source-file, remove the blanks, reverse the contents and save it into destination file\n\n");
		return 2;
	}
	sf = fopen(argv[1], "r");
	if (sf == NULL) {
		printf("Error: Can not open file `%s` for reading.\n", argv[1]);
		return 1;
	}
	fseek(sf, 0, SEEK_END);
	slen = ftell(sf);
	fseek(sf, 0, SEEK_SET);
	s = malloc(slen);
	if (s == NULL) {
		printf("Error: Can not allocate enough memory.\n");
		return 2;
	}
	i = 0;
	while ((c = fgetc(sf)) != EOF) {
		if (c != ' ')
			s[i++] = c;
	}
	dlen = i;
	for (i = 0, j = dlen - 1; i < j; i++, j--)
		c = s[i], s[i] = s[j], s[j] = c;
	df = fopen(argv[2], "w");
	if (df == NULL) {
		printf("Error: Can not open file `%s` for writing.\n", argv[2]);
		free(s);
		return 3;
	}
	for (i = 0; i < dlen; i++)
		fputc(s[i], df);
	fclose(sf);
	fclose(df);
	puts("Success");
	return 0;
}

