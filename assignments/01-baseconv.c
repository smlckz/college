#include <stdio.h>
#include <string.h>
#include <math.h>

#define LINESZ 100

struct num {
	double whole_part, frac_part;
};

void read_line(char *s, size_t len)
{
	size_t n;
	if (!fgets(s, len, stdin)) return;
	n = strlen(s);
	if (s[n - 1] == '\n') s[n - 1] = '\0';
}

int chrval(char c, int base)
{
	if (base <= 10)
		return ('0' <= c && c <= ('0'-1+base)) ? c - '0' : -1;
	else if (base <= 36)
		return ('0' <= c && c <= '9')
			? c - '0'
			: ('a' <= c && c <= ('a'-1+base))
			? c - 'a' + 10
			: ('A' <= c && c <= ('A'-1+base))
			? c - 'A' + 10
			: -1;
	else
		return -1;
}

int parse_num(char *s, struct num *p, int base)
{
	double n = 0, ad = 0;
	int d;
	double pos = 1;
	for (; *s != '\0' && *s != '.'; s++) {
		d = chrval(*s, base);
		if (d == -1) return 1;
		n *= base;
		n += d;
	}
	p->whole_part = n;
	if (*s != '.') {
		p->frac_part = 0;
		return 0;
	}
	for (s++; *s != '\0'; s++) {
		d = chrval(*s, base);
		if (d == -1) return 1;
		pos /= base;
		ad += d * pos;
	}
	p->frac_part = ad;
	return 0;
}

char valchr(int d, int base)
{
	if (base <= 10)
		return '0'+d;
	else if (base <= 36)
		return d < 10 ? '0' + d : 'A' + d - 10;
}

void print_whole_part(double whole_part, int base)
{
	double m, n;
	int z;
	m = n = whole_part;
	if (n < base) {
		putchar(valchr(n, base));
		return;
	}
	for (z = 0; fmod(m, base) == 0; m = trunc(m / base), z++);
	for (m = 0; n != 0; n = trunc(n / base))
		m *= base, m += fmod(n, base);
	for (; m != 0; m = trunc(m / base))
		putchar(valchr(fmod(m, base), base));
	while (z --> 0) putchar('0');
}

void print_frac_part(double frac_part, int base)
{
	double m = frac_part;
	int d;
	putchar('.');
	while (m > 0) {
		m *= base;
		d = trunc(m);
		putchar(valchr(d, base));
		m -= d;
	}
}

int yes(void)
{
	char c;
	printf("Try again? (y/n): ");
	scanf("%c", &c);
	printf("\n\n");
	if (c == 'y' || c == 'Y')
		return 1;
	else if (c == 'n' || c == 'N')
		return 0;
	printf("Assuming no\n\n");
	return 0;
}

int main(void)
{
	char line[LINESZ];
	int inbase, outbase;
	struct num n;
	do {
		printf("Bases: \n"
		       " 2  Binary\n 8  Octal\n10  Decimal\n16  Hexadecimal\n\n");
		printf("Choose input base: ");
		scanf("%i%*c", &inbase);
		if (inbase <= 0 || inbase > 36) {
			printf("Invalid input base %i\n", inbase);
			continue;
		}
		printf("Enter the number in base %i: ", inbase);
		read_line(line, LINESZ);
		if (parse_num(line, &n, inbase)) {
			printf("Invalid input in base %i: %s\n", inbase, line);
			continue;
		}
		printf("Choose output base: ");
		scanf("%i%*c", &outbase);
		if (outbase <= 0 || outbase > 36) {
			printf("Invalid output base %i\n", outbase);
			continue;
		}
		printf("The given number in base %i is ", outbase);
		print_whole_part(n.whole_part, outbase);
		if (n.frac_part != 0) print_frac_part(n.frac_part, outbase);
		printf("\n\n");
	} while (yes());
	puts("Bye");
	return 0;
}

