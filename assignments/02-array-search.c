#include <stdio.h>
#include <stdlib.h>

int *read_array(int *len)
{
	int i;
	int *arr;
	printf("Enter the number of elements: ");
	scanf("%d", len);
	arr = malloc(*len * sizeof(int));
	if (arr == NULL) {
		printf("Could not allocate enough memory to hold the array\n");
		return NULL;
	}
	printf("Enter the elements: ");
	for (i = 0; i < *len; i++) {
		scanf("%d", &arr[i]);
	}
	return arr;
}

void print_array(int *arr, int len)
{
	int i;
	if (len == 0) {
		printf("(empty array)\n");
		return;
	}
	for (i = 0; i < len; i++) {
		printf(" %d", arr[i]);
	}
	printf("\n");
}

int compare_int(const void *a, const void *b)
{
	return *(const int *)a - *(const int *)b;
}

int linear_search(int *arr, int len, int elem)
{
	int i;
	for (i = 0; i < len; i++) {
		if (arr[i] == elem)
			return i;
	}
	return -1;
}

int binary_search(int *arr, int len, int elem)
{
	int left, right, mid;
	left = 0;
	right = len - 1;
	while (left <= right) {
		mid = (left + right) / 2;
		if (arr[mid] < elem)
			left = mid + 1;
		if (elem < arr[mid])
			right = mid - 1;
		if (arr[mid] == elem)
			return mid;
	}
	return -1;
}

int main(void)
{
	int *arr, len, elem, choice, pos;
	printf("Searches for an element in a given array\n\n");
	printf("Enter the array:\n");
	arr = read_array(&len);
	if (len == -1) {
		return 1;
	}
	printf("Choose the method of searching:\n"
		" 1  Linear search\n"
		" 2  Binary search\n"
		"Enter choice: ");
	scanf("%d", &choice);
	switch (choice) {
	case 1:
		printf("The elements of the array:");
		print_array(arr, len);
		printf("Enter the element to find in the array: ");
		scanf("%d", &elem);
		pos = linear_search(arr, len, elem);
		break;
	case 2:
		qsort(arr, len, sizeof(int), &compare_int);
		printf("After sorting, the elements of the array:");
		print_array(arr, len);
		printf("Enter the element to find in the sorted array: ");
		scanf("%d", &elem);
		pos = binary_search(arr, len, elem);
		break;
	default:
		printf("Invalid choice of method of searching.\n");
		choice = 0;
	}
	if (choice) {
		if (pos > -1) {
			printf("The element %d was found in the array at position %d\n",
				elem, pos);
		} else {
			printf("The element %d was not found in the array.\n", elem);
		}
	}
	free(arr);
	return 0;
}

