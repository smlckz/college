#include <stdio.h>

int main(void)
{
	int arr[10][10], row, col, i, j;
	printf("Enter the number of rows and columns you want to enter: ");
	scanf("%i%i", &row, &col);
	printf("Enter the %d elements:\n", row * col);
	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			scanf("%d", &arr[i][j]);
		}
	}
	puts("The elements are:");
	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			printf("%d\t", arr[i][j]);
		}
		printf("\n");
	}
	return 0;
}

