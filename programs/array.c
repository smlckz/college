#include <stdio.h>

#define MAXLEN 100

int main(void)
{
	int a[MAXLEN], sz, i;
	printf("Enter the length of the array: ");
	scanf("%d", &sz);
	if (sz < 0) {
		printf("Invalid length of array.\n");
		return 1;
	}
	if (sz > MAXLEN) {
		printf("The maximum length of array is %d.\n", MAXLEN);
		return 1;
	}
	printf("Enter the elements of the array: ");
	for (i = 0; i < sz; i++) {
		scanf("%d", &a[i]);
	}
	printf("The elements of the array are: ");
	for (i = 0; i < sz; i++) {
		printf("%d ", a[i]);
	}
	printf("\n");
}

