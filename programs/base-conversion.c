#include <math.h>
#include <stdio.h>
#include <string.h>

#define LINESZ 100

void read_line(char *s, size_t len)
{
	size_t n;
	if (!fgets(s, len, stdin)) return;
	n = strlen(s);
	if (s[n - 1] == '\n') s[n - 1] = '\0';
}

long chrval(char c, int base)
{
	if (base <= 10)
		return ('0' <= c && c <= ('0'-1+base)) ? c - '0' : -1;
	else if (base <= 36)
		return ('0' <= c && c <= '9')
			? c - '0'
			: ('a' <= c && c <= ('a'-1+base))
			? c - 'a' + 10
			: ('A' <= c && c <= ('A'-1+base))
			? c - 'A' + 10
			: -1;
	else
		return -1;
}

int parse_num(char *s, double *p, int base)
{
	double n = 0, d;
	for (; *s != '\0'; s++) {
		d = chrval(*s, base);
		if (d == -1) return 1;
		n *= base;
		n += d;
	}
	*p = n;
	return 0;
}

char valchr(long d, int base)
{
	if (base <= 10)
		return '0'+d;
	else if (base <= 36)
		return d < 10 ? '0' + d : 'A' + d - 10;
}

void print_integral_part(double n, int base)
{
	double m = n, d, z;
	if (n < base) {
		putchar(valchr(n, base));
		return;
	}
	for (z = 0; fmod(m, base) == 0; m /= base, z++);
	for (m = 0; n != 0; n /= base)
		m *= base, m += fmod(n, base);
	for (; m != 0; m /= base)
		putchar(valchr(fmod(m, base), base));
	while (z --> 0) putchar('0');
}

void print_fractional_part(double n, int base)
{
	double m = n, d, z;
	putchar('.');
	while (m != 0) {
		m *= base;
		d = fmod(m, base);
		putchar(valchar(d, base));
		
	}
}

int yes(void)
{
	char c;
	printf("Try again? (y/n): ");
	scanf("%c", &c);
	printf("\n\n");
	if (c == 'y' || c == 'Y')
		return 1;
	else if (c == 'n' || c == 'N')
		return 0;
	printf("Assuming no\n\n");
	return 0;
}

int main(void)
{
	char line[LINESZ];
	int inbase, outbase;
	double n;
	do {
		printf("Bases: \n"
		       " 2  Binary\n 8  Octal\n10  Decimal\n16  Hexadecimal\n\n");
		printf("Choose input base: ");
		scanf("%i%*c", &inbase);
		if (inbase <= 0 || inbase > 36) {
			printf("Invalid input base %i\n", inbase);
			continue;
		}
		printf("Enter the number in base %i: ", inbase);
		read_line(line, LINESZ);
		if (parse_num(line, &n, inbase)) {
			printf("Invalid input in base %i: %s\n", inbase, line);
			continue;
		}
		printf("Choose output base: ");
		scanf("%i%*c", &outbase);
		if (outbase <= 0 || outbase > 36) {
			printf("Invalid output base %i\n", outbase);
			continue;
		}
		printf("The given number in base %i is ", outbase);
		print_num(n, outbase);
		printf("\n\n");
	} while (yes());
	puts("Bye");
	return 0;
}

