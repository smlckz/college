#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int value_of_char(int ch, unsigned base)
{
	if (base <= 10) {
		if ('0' <= ch && ch <= '0' + base - 1)
			return ch - '0';
		else
			return -1;
	} else if (base >= 11 && base <= 36) {
		if ('0' <= ch && ch <= '9')
			return ch - '0';
		else if ('a' <= ch && ch <= 'a' + base - 11)
			return ch - 'a' + 10;
		else if ('A' <= ch && ch <= 'A' + base - 11)
			return ch - 'A' + 10;
		else
			return -1;
	} else {
		/* unreachable */
		abort();
	}
}

int char_from_value(unsigned digit)
{
	if (digit <= 9) {
		return '0' + digit;
	} else if (10 <= digit && digit <= 36) {
		return 'A' + digit - 10;
	} else {
		/* unreachable */
		abort();
	}
}

int parse_number(char *s, unsigned base, unsigned long *number)
{
	unsigned long n = 0;
	int digit;
	for (; *s != '\0'; s++) {
		digit = value_of_char(*s, base);
		if (digit == -1)
			return -1;
		n = n * base + (unsigned)digit;
	}
	*number = n;
	return 0;
}

int number_of_digits(unsigned long number, unsigned base)
{
	int n;
	if (number == 0) return 1;
	for (n = 0; number != 0; n++) {
		number = number / base;
	}
	return n;
}

void format_number(unsigned long number, unsigned base, char *s)
{
	unsigned digit;
	int i;
	int n = number_of_digits(number, base);
	s[n] = '\0';
	i = n - 1;
	do {
		digit = number % base;
		number = number / base;
		s[i] = char_from_value(digit);
		i = i - 1;
	} while (number != 0);
}

int main(void)
{
	char input[100], output[100];
	unsigned long number;
	unsigned input_base, output_base;
	printf("Base conversion\n"
		"Supported: 2 <= base <= 36\n\n");
	printf("Enter the number: ");
	fgets(input, sizeof(input), stdin);
	input[strlen(input) - 1] = '\0';
	printf("Enter the base of the number input: ");
	scanf("%u", &input_base);
	if (input_base < 2) {
		printf("Invalid input base %u, must be >= 2\n", input_base);
		return 1;
	} else if (input_base > 36) {
		printf("Unsupported input base %u\n", input_base);
		return 2;
	} else if (strlen(input) == 0) {
		printf("Invalid input, input must not be empty\n");
		return 3;
	}
	if (parse_number(input, input_base, &number)) {
		printf("Invalid input, contains character(s) which does not represent a digit of base %u\n", input_base);
		return 4;
	}
	printf("Enter the base of the output: ");
	scanf("%u", &output_base);
	if (output_base < 2) {
		printf("Invalid output base %u, must be >= 2\n", output_base);
		return 5;
	} else if (input_base > 36) {
		printf("Unsupported output base %u\n", output_base);
		return 6;
	}
	format_number(number, output_base, output);
	printf("%s in base %u = %s in base %u\n", input, input_base, output, output_base);
	return 0;
}

