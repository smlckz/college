#include <stdio.h>
#include <stdlib.h>

int read_array(int **arr)
{
	int i, len;
	printf("Enter the number of elements: ");
	scanf("%d", &len);
	*arr = malloc(len * sizeof(int));
	if (*arr == NULL) {
		printf("Could not allocate enough memory to hold the array\n");
		return -1;
	}
	printf("Enter the elements: ");
	for (i = 0; i < len; i++) {
		scanf("%d", *arr + i);
	}
	return len;
}

int binary_search(int *arr, int len, int item)
{
	int left, right, mid;
	left = 0;
	right = len - 1;
	while (left <= right) {
		mid = (left + right) / 2;
		if (arr[mid] < item)
			left = mid + 1;
		else if (arr[mid] > item)
			right = mid - 1;
		else
			return mid;
	}
	return -1;
}

int main(void)
{
	int *arr, len, item, pos, i;
	len = read_array(&arr);
	printf("The elements of the array: \n");
	for (i = 0; i < len; i++) {
		printf(" %d", arr[i]);
	}
	printf("\nEnter the item to find in the given array: ");
	scanf("%d", &item);
	pos = binary_search(arr, len, item);
	if (pos > -1) {
		printf("The item %d was found in the array at position %d.\n", item, pos);
	} else {
		printf("The item %d was not found in the array.\n", item);
	}
	return 0;
}

/*
Set 1: [zero length array]
Enter the number of elements: 0
Enter the elements: The elements of the array: 

Enter the item to find in the given array: 1
The item 1 was not found in the array.

Set 2.1: [directly middle of even length array]
Enter the number of elements: 6
Enter the elements: -4 -2 0 1 3 5
The elements of the array: 
 -4 -2 0 1 3 5
Enter the item to find in the given array: 0
The item 0 was found in the array at position 2.

Set 2.2: [not found in directly middle of even length array]
Enter the number of elements: 6
Enter the elements: -3 -2 -1 1 2 3
The elements of the array: 
 -3 -2 -1 1 2 3
Enter the item to find in the given array: 0
The item 0 was not found in the array.

Set 3.1: [right of middle of even length array]
Enter the number of elements: 6
Enter the elements: -3 -1 0 1 2 4
The elements of the array: 
 -3 -1 0 1 2 4
Enter the item to find in the given array: 2
The item 2 was found in the array at position 4.

Set 3.2: [not found in right of middle of even length array]


Set 4.1: [right edge of even length array]
Enter the number of elements: 6
Enter the elements: -4 -2 0 1 3 5
The elements of the array: 
 -4 -2 0 1 3 5
Enter the item to find in the given array: 5
The item 5 was found in the array at position 5.

Set 4.2: [outside of right edge of even length array]
Enter the number of elements: 6
Enter the elements: -5 -3 -1 0 2 4
The elements of the array: 
 -5 -3 -1 0 2 4
Enter the item to find in the given array: -10
The item -10 was not found in the array.

Set 5: [left of middle of even length array]
Enter the number of elements: 6
Enter the elements: -6 -4 -2 0 2 4
The elements of the array: 
 -6 -4 -2 0 2 4
Enter the item to find in the given array: -4
The item -4 was found in the array at position 1.

Set 6: [left edge of even length array]
Enter the number of elements: 6
Enter the elements: -7 -5 -3 -1 0 1
The elements of the array: 
 -7 -5 -3 -1 0 1
Enter the item to find in the given array: -7
The item -7 was found in the array at position 0.

Set 7: []

*/

