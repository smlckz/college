#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

struct bst_node {
	int data;
	struct bst_node *left, *right;
};

struct bst {
	int id;
	int nodes;
	struct bst_node *tree;
	struct bst *prev, *next;
};

struct forest {
	int population;
	struct bst *first_tree, *last_tree;
};

struct bst *bst_create(struct forest *forest, int tree_id)
{
	struct bst *new_bst = malloc(sizeof(*new_bst));
	if (new_bst == NULL) {
		puts("Can not allocate enough memory for a new tree\n");
		abort();
	}
	new_bst->id = tree_id;
	new_bst->tree = NULL;
	if (forest->population == 0) {
		new_bst->prev = new_bst->next = NULL;
		forest->first_tree = forest->last_tree = new_bst;
	} else {
		forest->last_tree->next = new_bst;
		new_bst->prev = forest->last_tree;
		forest->last_tree = new_bst;
	}
	forest->population += 1;
	return new_bst;
}

bool yes(const char *prompt)
{
	char choice;
	for (;;) {
		printf("%s (y/n) ", prompt);
		if (scanf(" %c", &choice) != 1) {
			scanf("%*[^\n]%*c");
			continue;
		}
		switch (choice) {
		case 'y': case 'Y':
			return 1;
		case 'n': case 'N':
			return 0;
		}
	}
}

struct bst_node *bst_create_node(int new_data)
{
	struct bst_node *node = malloc(sizeof(*node));
	if (node == NULL) {
		puts("Could not allocate enough space to create a new node for the tree\n");
		abort();
	}
	*node = (struct bst_node){
		.data = new_data,
		.left = NULL, .right = NULL
	};
	return node;
}

void bst_display_indented(struct bst_node *node, int level)
{
	if (node->left != NULL) {
		bst_display_indented(node->left, level + 1);
	}
	for (int i = 0; i < level; i++) {
		putchar('\t');
	}
	if (node == NULL) {
		printf("\n");
		return;
	}
	printf("%d\n", node->data);
	if (node->right != NULL) {
		bst_display_indented(node->right, level + 1);
	}
}

void bst_display(struct bst *bst, bool cont)
{
	const char *start = cont ? ", the" : "The";
	if (bst->nodes == 0) {
		printf("%s BST %d has no elements in it\n", start, bst->id);
		return;
	}
	printf("%s BST %d now has the following elements:\n", start, bst->id);
	bst_display_indented(bst->tree, 0);
	printf("\n");
}

void bst_insert_recursive(struct bst_node **node, int new_data)
{
	if (*node != NULL) {
		int data = (*node)->data;
		if (new_data < data) {
			bst_insert_recursive(&(*node)->left, new_data);
		} else { /* new_data >= data */
			bst_insert_recursive(&(*node)->right, new_data);
		}
		return;
	}
	*node = bst_create_node(new_data);
}

void bst_insert_iterative(struct bst_node **node, int new_data)
{
	while (*node != NULL) {
		int data = (*node)->data;
		if (new_data < data) {
			node = &(*node)->left;
		} else { /* new_data >= data */
			node = &(*node)->right;
		}
	}
	*node = bst_create_node(new_data);
}

int bst_insert_nodes(struct bst *bst)
{
	printf("Choose 1 for recursive insertion, 2 for iterative insertion\n");
	printf("Enter your choice: ");
	int choice;
	scanf("%d", &choice);
	void (*insert)(struct bst_node **, int) = NULL;
	if (choice == 1) insert = bst_insert_recursive;
	else if (choice == 2) insert = bst_insert_iterative;
	if (insert == NULL) {
		printf("Invalid choice,  try again\n");
		return -1;
	}
	printf("== Insert into Tree %d ==\n", bst->id);
	printf("Enter the number of elements you want to insert: ");
	int count;
	scanf("%d", &count);
	if (count < 0) {
		printf("Can not insert negative number of elements\n");
		return -1;
	}
	printf("Enter the elements: ");
	for (int i = 0; i < count; i++) {
		int element;
		scanf("%d", &element);
		insert(&bst->tree, element);
		bst->nodes += 1;
		printf("After inserting %d", element);
		bst_display(bst, true);
	}
	return 0;
}

struct bst_node **bst_search(struct bst_node **node, int data_to_find)
{
	while (*node != NULL) {
		int data = (*node)->data;
		if (data_to_find < data) {
			node = &(*node)->left;
		} else if (data_to_find == data) {
			return node;
		} else { /* new_data > data */
			node = &(*node)->right;
		}
	}
	return NULL;
}

struct bst_node *bst_pluck_inorder_successor(struct bst_node *node)
{
	struct bst_node **ptr = &node->right;
	while ((*ptr)->left != NULL) {
		ptr = &(*ptr)->left;
	}
	struct bst_node *tmp = *ptr;
	*ptr = (*ptr)->right;
	return tmp;
}

int bst_delete(struct bst_node **node, int data)
{
	node = bst_search(node, data);
	if (node == NULL) {
		return -1;
	}
	if ((*node)->left == NULL && (*node)->right == NULL) {
		free(*node);
		*node = NULL;
		return 0;
	}
	void *tmp = *node;
	if ((*node)->left == NULL) {
		*node = (*node)->right;
		free(tmp);
	} else if ((*node)->right == NULL) {
		*node = (*node)->left;
		free(tmp);
	} else {
		struct bst_node *successor = bst_pluck_inorder_successor(*node);
		successor->left = (*node)->left;
		successor->right = (*node)->right;
		*node = successor;
		free(tmp);
	}
	return 0;
}

int bst_delete_nodes(struct bst *bst)
{
	printf("== Delete from Tree %d ==\n", bst->id);
	printf("Enter the number of elements to delete: ");
	int count;
	scanf("%d", &count);
	if (count < 0) {
		printf("Can not insert negative number of elements\n");
		return -1;
	}
	printf("Enter the elements: ");
	for (int i = 0; i < count; i++) {
		int element;
		scanf("%d", &element);
		if (bst_delete(&bst->tree, element)) {
			printf("%d was not found in the tree\n", element);
		} else {
			bst->nodes -= 1;
			printf("After deleting %d", element);
			bst_display(bst, true);
		}
	}
	return 0;
}

void bst_display_preorder_recursive(struct bst_node *node)
{
	if (node == NULL)
		return;
	printf("%d ", node->data);
	bst_display_preorder_recursive(node->left);
	bst_display_preorder_recursive(node->right);
}

void bst_display_inorder_recursive(struct bst_node *node)
{
	if (node == NULL)
		return;
	bst_display_inorder_recursive(node->left);
	printf("%d ", node->data);
	bst_display_inorder_recursive(node->right);
}

void bst_display_postorder_recursive(struct bst_node *node)
{
	if (node == NULL)
		return;
	bst_display_postorder_recursive(node->left);
	bst_display_postorder_recursive(node->right);
	printf("%d ", node->data);
}

int bst_height(struct bst_node *node)
{
	if (node == NULL) return 0;
	int left_height = bst_height(node->left), right_height = bst_height(node->right);
	return 1 + (left_height < right_height ? right_height : left_height);
}

void bst_display_nodes(struct bst *bst)
{
	printf("== Display BST %d ==\n", bst->id);
	printf("Menu:\n"
	       "1. Preorder traversal, recursively\n"
	       "2. Inorder traversal, recursively\n"
	       "3. Postorder traversal, recursively\n"
	       "4. Preorder traversal, iteratively\n"
	       "5. Inorder traversal, iteratively\n"
	       "6. Postorder traversal, iteratively\n"
	       "7. Level-by-level traversal\n\n");
	printf("Enter your choice: ");
	int choice;
	scanf("%d", &choice);
	void (*display)(struct bst_node *) = NULL;
	switch (choice) {
	case 1:
		display = bst_display_preorder_recursive;
		break;
	case 2:
		display = bst_display_inorder_recursive;
		break;
	case 3:
		display = bst_display_postorder_recursive;
		break;
	default:
		puts("Invalid choice, try again");
	}
	if (display != NULL) {
		printf("Traversing BST %d and printing it as chosen: ", bst->id);
		display(bst->tree);
	}
}

int bst_count_leaves(struct bst_node *node)
{
	if (node == NULL) return 0;
	if (node->left == NULL && node->right == NULL)
		return 1;
	return bst_count_leaves(node->left) + bst_count_leaves(node->right);
}

int bst_count_non_leaves(struct bst_node *node)
{
	if (node == NULL || (node->left == NULL && node->right == NULL))
		return 0;
	return 1 + bst_count_non_leaves(node->left) + bst_count_non_leaves(node->right);
}

void bst_mirror(struct bst_node *from, struct bst_node **to)
{
	if (from == NULL) {
		*to = NULL;
		return;
	}
	*to = bst_create_node(from->data);
	bst_mirror(from->left, &(*to)->right);
	bst_mirror(from->right, &(*to)->left);
}

bool bst_compare(struct bst_node *a, struct bst_node *b)
{
	if (a == NULL && b == NULL)
		return true;
	if (a->data != b->data)
		return false;
	if (bst_compare(a->left, b->left) && bst_compare(a->right, b->right))
		return true;
	return false;
}

void forest_overview(struct forest *f)
{
	printf("There are %d trees out there.\n", f->population);
	struct bst *ptr = f->first_tree;
	puts("The trees are:");
	while (ptr != NULL) {
		printf("BST %d, with %d elements\n", ptr->id, ptr->nodes);
		ptr = ptr->next;
	}
	printf("\n");
}

struct bst *forest_find(struct forest *f, int id)
{
	struct bst *ptr = f->first_tree;
	while (ptr != NULL) {
		if (ptr->id == id)
			return ptr;
		ptr = ptr->next;
	}
	return NULL;
}

void bst_free_node(struct bst_node *t)
{
	if (t == NULL) return;
	bst_free_node(t->left);
	bst_free_node(t->right);
	free(t);
}

struct bst *forest_extirpate_tree(struct forest *f, struct bst *t)
{
	f->population -= 1;
	struct bst *new_current_bst = NULL;
	if (t->next != NULL) {
		new_current_bst = t->next;
	} else {
		new_current_bst = t->prev;
	}
	if (f->first_tree == t) {
		f->first_tree = t->next;
	}
	if (f->last_tree == t) {
		f->last_tree = t->prev;
	}
	if (t->next != NULL)
		t->next->prev = t->prev;
	if (t->prev != NULL)
		t->prev->next = t->next;
	bst_free_node(t->tree);
	free(t);
	return new_current_bst;
}

void forest_free(struct forest *f)
{
	struct bst *t = f->first_tree, *tmp;
	while (t != NULL) {
		tmp = t->next;
		bst_free_node(t->tree);
		free(t);
		t = tmp;
	}
	f->population = 0;
}

int main(void)
{
	int new_tree_id = 1;
	struct forest forest = {
		.population = 0,
		.first_tree = NULL,
		.last_tree = NULL,
	};
	struct bst *current_bst = NULL;
	puts("Binary Search Tree operations\n");
	for (;;) {
		puts("\nMenu:\n"
		     " 1. Create a new BST\n"
		     " 2. Insert into BST\n"
		     " 3. Delete from BST\n"
		     " 4. Search in BST\n"
		     " 5. Display the BST\n"
		     " 6. Count the number of leaf and non-leaf nodes of the BST\n"
		     " 7. Find the height of the BST\n"
		     " 8. Create mirror image of this BST\n"
		     " 9. Compare two BSTs for equality\n"
		     "10. Change current BST\n"
		     "11. Extirpate current BST\n"
		     " 0. Exit\n");
		printf("Enter your choice: ");
		int choice;
		if (scanf("%d", &choice) != 1) {
			puts("Invalid choice, try again\n");
			/* Discard the rest of the input line */
			scanf("%*[^\n]%*c");
			continue;
		}
		switch (choice) {
		case 1:
		case 2:
			if (choice == 1 || current_bst == NULL) {
				printf("Creating Tree %d... ", new_tree_id);
				current_bst = bst_create(&forest, new_tree_id++);
				puts("done.");
			}
			if (choice == 1 && !yes("Do you want to insert into this BST?"))
				break;
			if (bst_insert_nodes(current_bst))
				break;
			break;
		case 3:
			if (current_bst == NULL) {
				puts("There are no trees out there, so nothing to delete from\n");
				break;
			}
			if (current_bst->nodes == 0) {
				puts("The current tree do not have any nodes to delete from\n");
				break;
			}
			if (bst_delete_nodes(current_bst))
				break;
			break;
		case 4:
			if (current_bst == NULL) {
				puts("There are no trees out there, so nothing to search for\n");
				break;
			}
			bst_display(current_bst, false);
			printf("Enter the element to search: ");
			int element;
			scanf("%d", &element);
			void *ptr = bst_search(&current_bst->tree, element);
			printf("The element %d was%s found in Tree %d\n", element,
				ptr == NULL ? " not" : "", current_bst->id);
			break;
		case 5:
			if (current_bst == NULL) {
				puts("There are no trees out there, so nothing to display\n");
				break;
			}
			bst_display_nodes(current_bst);
			break;
		case 6:
			if (current_bst == NULL) {
				puts("There are no trees out there\n");
				break;
			}
			printf("The current BST %d has %d leaves and %d non-leaf, internal nodes\n",
				current_bst->id,
				bst_count_leaves(current_bst->tree),
				bst_count_non_leaves(current_bst->tree));
			break;
		case 7:
			if (current_bst == NULL) {
				puts("There are no trees out there\n");
				break;
			}
			printf("The height of the current BST %d is %d\n", current_bst->id, bst_height(current_bst->tree));
			break;
		case 8:
			if (current_bst == NULL) {
				puts("There are no trees out there to mirror\n");
				break;
			}
			printf("Creating Tree %d... ", new_tree_id);
			struct bst *mirrored_bst = bst_create(&forest, new_tree_id++);
			puts("done.");
			printf("Creating a mirror of BST %d in BST %d...\n", current_bst->id, mirrored_bst->id);
			bst_mirror(current_bst->tree, &mirrored_bst->tree);
			bst_display(current_bst, false);
			bst_display(mirrored_bst, false);
			break;
		case 9:
			if (current_bst == NULL) {
				puts("There are no trees out there to compare between\n");
				break;
			}
			forest_overview(&forest);
			printf("Current tree is BST %d\n", current_bst->id);
			printf("Enter the ID of tree to compare: ");
			int id;
			scanf("%d", &id);
			struct bst *another_bst = forest_find(&forest, id);
			if (another_bst == NULL) {
				printf("Tree with ID %d was not found\n", id);
				break;
			}
			printf("BST %d is%s equal to BST %d\n", current_bst->id,
				bst_compare(current_bst->tree, another_bst->tree) ? "" : " not",
				another_bst->id);
			break;
		case 10:
			if (current_bst == NULL) {
				puts("There are no trees out there, no current tree to change\n");
				break;
			}
			forest_overview(&forest);
			printf("Current tree has ID %d\n", current_bst->id);
			printf("Enter the ID of the new current tree: ");
			int new_bst_id;
			scanf("%d", &new_bst_id);
			struct bst *t = forest_find(&forest, new_bst_id);
			if (t == NULL) {
				printf("Tree with ID %d was not found\n", id);
				break;
			}
			current_bst = t;
			printf("Current tree is the BST %d\n", new_bst_id);
			break;
		case 11:
			if (current_bst == NULL) {
				puts("There are no trees out there, no tree to extirpate\n");
				break;
			}
			if (!yes("Are you sure you want to extirpate the current tree?"))
				break;
			printf("Extirpating BST %d...", current_bst->id);
			current_bst = forest_extirpate_tree(&forest, current_bst);
			puts("done.");
			if (current_bst != NULL) {
				printf("Current tree is now BST %d\n", current_bst->id);
			} else {
				printf("No more trees left\n");
			}
			break;
		case 0:
			forest_free(&forest);
			puts("Bye");
			return 0;
		default:
			puts("Invalid choice, Try again.");
		}
	}
}

