#include <stdio.h>
#include <stdlib.h>

int read_array(int **arr)
{
	int i, len;
	printf("Enter the number of elements: ");
	scanf("%d", &len);
	*arr = malloc(len * sizeof(int));
	if (*arr == NULL) {
		printf("Could not allocate enough memory to hold the array\n");
		return -1;
	}
	printf("Enter the elements: ");
	for (i = 0; i < len; i++) {
		scanf("%d", *arr + i);
	}
	return len;
}

void print_array(int *arr, int n)
{
	int i;
	if (n == 0) {
		printf(" (empty array)");
	}
	for (i = 0; i < n; i++) {
		printf(" %d", arr[i]);
	}
	printf("\n");
}

void swap(int *a, int *b)
{
	int tmp = *b;
	*b = *a;
	*a = tmp;
}

void bubble_sort(int *arr, int len)
{
	int i, j;
	for (i = 0; i < len - 1; i++) {
		for (j = 0; j < len - i - 1; j++) {
			if (arr[j] > arr[j+1]) {
				swap(&arr[j], &arr[j+1]);
			}
		}
	}
}

int main(void)
{
	int *arr, len;
	len = read_array(&arr);
	if (len == -1) return 1;
	printf("Before sorting, the array elements are: ");
	print_array(arr, len);
	bubble_sort(arr, len);
	printf("After sorting, the array elements are: ");
	print_array(arr, len);
	return 0;
}


/*
Set 0:
Enter the number of elements: 0
Enter the elements: Before sorting, the array elements are:  (empty array)
After sorting, the array elements are:  (empty array)

Set 1:
Enter the number of elements: 9
Enter the elements: -1 2 0 -9 7 6 4 -4 3  
Before sorting, the array elements are:  -1 2 0 -9 7 6 4 -4 3
After sorting, the array elements are:  -9 -4 -1 0 2 3 4 6 7

*/

