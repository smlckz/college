#include <stdio.h>
#include <stdlib.h>

typedef struct node {
	int data;
	struct node *next;
} node;

node *create_node(int data, node *next)
{
	node *newnode = malloc(sizeof(node));
	if (newnode == NULL) {
		fprintf(stderr, "Could not allocate space for node.\n");
		abort();
	}
	newnode->data = data;
	newnode->next = next;
	return newnode;
}

int length(node *list)
{
	int len = 0;
	node *ptr = list;
	if (list == NULL) {
		return 0;
	}
	do {
		len = len + 1;
		ptr = ptr->next;
	} while (ptr != list);
	return len;
}


node *insert_at_beginning(node *list, int item)
{
	node *newnode = create_node(item, list);
	node *ptr = list;
	if (list == NULL) {
		newnode->next = newnode;
		return newnode;
	}
	while (ptr->next != list) {
		ptr = ptr->next;
	}
	ptr->next = newnode;
	return newnode;
}

node *insert_at_end(node *list, int item)
{
	node *ptr = list;
	if (list == NULL) {
		return create_node(item, NULL);
	}
	while (ptr->next != list) {
		ptr = ptr->next;
	}
	ptr->next = create_node(item, list);
	return list;
}

node *insert(node *list, int index, int item)
{
	int len = length(list);
	int i;
	node *ptr, *tmp;
	if (index < 1 || index > len + 1) {
		printf("Invalid index %i, can not insert\n", index);
		return list;
	}
	if (index == 1) {
		return insert_at_beginning(list, item);
	} else if (index == len + 1) {
		return insert_at_end(list, item);
	}
	ptr = list;
	for (i = 1; i < index - 1; i = i + 1) {
		ptr = ptr->next;
	}
	tmp = create_node(item, ptr->next);
	ptr->next = tmp;
	return list;
}

node *delete_at_beginning(node *list)
{
	node *rest, *tmp;
	if (list == NULL) {
		puts("Can not delete node from empty list");
		return NULL;
	}
	rest = list->next;
	if (rest == list) {
		rest = NULL;
	} else {
		tmp = rest;
		while (tmp->next != list) {
			tmp = tmp->next;
		}
		tmp->next = rest;
	}
	printf("The deleted item is: %i\n", list->data);
	free(list);
	return rest;
}

node *delete_at_end(node *list)
{
	node *ptr, *t;
	int item;
	if (list == NULL) {
		puts("Can not delete node from empty list");
		return NULL;
	} else if (list->next == list) {
		item = list->data;
		free(list);
		list = NULL;
	} else {
		ptr = list;
		while (ptr->next->next != list) {
			ptr = ptr->next;
		}
		t = ptr->next;
		item = t->data;
		free(t);
		ptr->next = list;
	}
	printf("The deleted item is: %i\n", item);
	return list;
}

node *delete(node *list, int index)
{
	int i, item, len;
	node *ptr, *t;
	len = length(list);
	if (index < 1 || index > len) {
		printf("Invalid index %i, can not delete\n", index);
		return list;
	}
	if (index == 1) {
		return delete_at_beginning(list);
	} else if (index == len) {
		return delete_at_end(list);
	}
	ptr = list;
	for (i = 1; i < index - 1; i = i + 1) {
		ptr = ptr->next;
	}
	t = ptr->next;
	printf("The deleted item is: %i\n", t->data);
	ptr->next = t->next;
	free(t);
	return list;
}

void display(node *list)
{
	int len = length(list);
	node *ptr = list;
	if (list == NULL) {
		puts("Empty list");
		return;
	}
	printf("The list elements are:");
	do {
		printf(" %i", ptr->data);
		ptr = ptr->next;
	} while (ptr != list);
	printf("\n");
	printf("The number of elements is %i\n", len);
}

int main(void)
{
	int choice, index, item;
	node *list = NULL;
	printf("Circular Linked list\n\n");
	while (1) {
		printf("Menu:\n"
		       " 1. Insert at beginning\n"
		       " 2. Insert at end\n"
		       " 3. Insert at given position\n"
		       " 4. Delete at beginning\n"
		       " 5. Delete at end\n"
		       " 6. Delete at given position\n"
		       " 7. Display\n"
		       " 0. Exit\n\n");
		printf("Enter your choice: ");
		scanf("%i", &choice);
		switch (choice) {
		case 1:
			printf("Enter the item to insert: ");
			scanf("%i", &item);
			list = insert_at_beginning(list, item);
			display(list);
			break;
		case 2:
			printf("Enter the item to insert: ");
			scanf("%i", &item);
			list = insert_at_end(list, item);
			display(list);
			break;
		case 3:
			printf("Enter the position to insert: ");
			scanf("%i", &index);
			printf("Enter the item to insert: ");
			scanf("%i", &item);
			list = insert(list, index, item);
			display(list);
			break;
		case 4:
			list = delete_at_beginning(list);
			display(list);
			break;
		case 5:
			list = delete_at_end(list);
			display(list);
			break;
		case 6:
			printf("Enter the position of node to delete: ");
			scanf("%i", &index);
			list = delete(list, index);
			display(list);
			break;
		case 7:
			display(list);
			break;
		case 0:
			puts("Bye");
			return 0;
		default:
			printf("Invalid choice. Try again\n");
			break;
		}
	}
}

