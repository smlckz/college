#include <stdio.h>
#include <stdlib.h>

typedef struct node {
	int data;
	struct node *prev, *next;
} node;

node *create_node(int data, node *prev, node *next)
{
	node *newnode = malloc(sizeof(node));
	if (newnode == NULL) {
		fprintf(stderr, "Could not allocate space for a new node.\n");
		abort();
	}
	newnode->data = data;
	newnode->prev = prev;
	if (prev != NULL) {
		prev->next = newnode;
	}
	newnode->next = next;
	if (next != NULL) {
		next->prev = newnode;
	}
	return newnode;
}

int length(node *list)
{
	int len = 0;
	while (list != NULL) {
		len = len + 1;
		list = list->next;
	}
	return len;
}


node *insert_at_beginning(node *list, int item)
{
	return create_node(item, NULL, list);
}

node *insert_at_end(node *list, int item)
{
	node *ptr = list;
	if (list == NULL) {
		return create_node(item, NULL, NULL);
	}
	while (ptr->next != NULL) {
		ptr = ptr->next;
	}
	create_node(item, ptr, NULL);
	return list;
}

node *insert(node *list, int index, int item)
{
	int len = length(list);
	int i;
	node *ptr, *tmp;
	if (index < 1 || index > len + 1) {
		printf("Invalid index %i, can not insert\n", index);
		return list;
	}
	if (index == 1) {
		return insert_at_beginning(list, item);
	} else if (index == len + 1) {
		return insert_at_end(list, item);
	}
	ptr = list;
	for (i = 1; i < index - 1; i = i + 1) {
		ptr = ptr->next;
	}
	create_node(item, ptr, ptr->next);
	return list;
}

node *delete_at_beginning(node *list)
{
	node *rest;
	if (list == NULL) {
		puts("Can not delete node from empty list");
		return NULL;
	}
	rest = list->next;
	printf("The deleted item is: %i\n", list->data);
	if (rest != NULL) {
		rest->prev = NULL;
	}
	free(list);
	return rest;
}

node *delete_at_end(node *list)
{
	node *ptr, *t;
	int item;
	if (list == NULL) {
		puts("Can not delete node from empty list");
		return NULL;
	} else if (list->next == NULL) {
		item = list->data;
		free(list);
		list = NULL;
	} else {
		ptr = list;
		while (ptr->next != NULL) {
			ptr = ptr->next;
		}
		t = ptr;
		ptr = t->prev;
		ptr->next = NULL;
		item = t->data;
		free(t);
	}
	printf("The deleted item is: %i\n", item);
	return list;
}

node *delete(node *list, int index)
{
	int i, item, len;
	node *ptr, *t, *tp, *tn;
	len = length(list);
	if (index < 1 || index > len) {
		printf("Invalid index %i, can not delete\n", index);
		return list;
	}
	if (index == 1) {
		return delete_at_beginning(list);
	} else if (index == len) {
		return delete_at_end(list);
	}
	ptr = list;
	for (i = 1; i < index; i = i + 1) {
		ptr = ptr->next;
	}
	t = ptr;
	tp = t->prev;
	tn = t->next;
	printf("The deleted item is: %i\n", t->data);
	/*
	t->prev->next = t->next;
	t->next->prev = t->prev;
	*/
	tp->next = tn;
	tn->prev = tp;
	free(t);
	return list;
}

void display(node *list)
{
	node *t;
	int len = length(list);
	if (list == NULL) {
		puts("Empty list");
		return;
	}
	printf("The list elements are:");
	while (list != NULL) {
		printf(" %i", list->data);
		t = list;
		list = list->next;
	}
	printf("\n");
	printf("The list elements in reverse are:");
	while (t != NULL) {
		printf(" %i", t->data);
		t = t->prev;
	}
	printf("\n");
	printf("The number of elements is %i\n", len);
}

int main(void)
{
	int choice, index, item;
	node *list = NULL;
	printf("Linked list\n\n");
	while (1) {
		printf("Menu:\n"
		       " 1. Insert at beginning\n"
		       " 2. Insert at end\n"
		       " 3. Insert at given position\n"
		       " 4. Delete at beginning\n"
		       " 5. Delete at end\n"
		       " 6. Delete at given position\n"
		       " 7. Display\n"
		       " 0. Exit\n\n");
		printf("Enter your choice: ");
		scanf("%i", &choice);
		switch (choice) {
		case 1:
			printf("Enter the item to insert: ");
			scanf("%i", &item);
			list = insert_at_beginning(list, item);
			display(list);
			break;
		case 2:
			printf("Enter the item to insert: ");
			scanf("%i", &item);
			list = insert_at_end(list, item);
			display(list);
			break;
		case 3:
			printf("Enter the position to insert: ");
			scanf("%i", &index);
			printf("Enter the item to insert: ");
			scanf("%i", &item);
			list = insert(list, index, item);
			display(list);
			break;
		case 4:
			list = delete_at_beginning(list);
			display(list);
			break;
		case 5:
			list = delete_at_end(list);
			display(list);
			break;
		case 6:
			printf("Enter the position of node to delete: ");
			scanf("%i", &index);
			list = delete(list, index);
			display(list);
			break;
		case 7:
			display(list);
			break;
		case 0:
			puts("Bye");
			return 0;
		default:
			printf("Invalid choice. Try again\n");
			break;
		}
	}
}

