#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STACK_MAX 100

int stack[STACK_MAX];
int top;

void push(int item)
{
	if (top == STACK_MAX - 1) {
		puts("Stack overflow!");
		exit(1);
	}
	top = top + 1;
	stack[top] = item;
}

int pop(void)
{
	int item;
	if (top == -1) {
		puts("Stack underflow!");
		exit(2);
	}
	item = stack[top];
	top = top - 1;
	return item;
}

#define INPUT_MAX 100

char input[INPUT_MAX];
int pos;

#define T_ERROR -1
#define T_END 0
#define T_NUMBER 1
#define T_OPERATOR 2

int toktype;

int next_token(void)
{
	int c = input[pos];
	int n, d;
	while (c == ' ' || c == '\t') {
		pos = pos + 1;
		c = input[pos];
	}
	switch (c) {
	case '\0':
		toktype = T_END;
		return 0;
	case '+': case '-':
	case '*': case '/':
	case '(': case ')':
		pos = pos + 1;
		toktype = T_OPERATOR;
		return c;
	}
	d = c - '0';
	if (d < 0 || d > 9) {
		toktype = T_ERROR;
		return 0;
	}
	n = 0;
	do {
		n = 10 * n + d;
		pos = pos + 1;
		c = input[pos];
		d = c - '0';
	} while (d >= 0 && d <= 9);
	toktype = T_NUMBER;
	return n;
}

int precedence(int op)
{
	switch (op) {
	case '+': case '-':
		return 1;
	case '*': case '/':
		return 2;
	default:
		/* unreachable */
		abort();
	}
}

#define OUTPUT_MAX 100

char output[OUTPUT_MAX];
int olen;

void output_character(int c)
{
	if (olen == OUTPUT_MAX - 1) {
		puts("Output buffer overflow!");
		exit(3);
	}
	output[olen] = c;
	olen = olen + 1;
}

void reverse(int *arr, int len)
{
	int i = 0, j = len - 1, t;
	while (i < j) {
		t = arr[i];
		arr[i] = arr[j];
		arr[j] = t;
		i = i + 1;
		j = j - 1;
	}
}

void output_number(int num)
{
	int a[31];
	int i, d, len, c;
	i = 0;
	do {
		d = num % 10;
		num = num / 10;
		a[i] = d;
		i = i + 1;
	} while (num > 0);
	len = i;
	reverse(a, len);
	i = 0;
	while (i < len) {
		c = a[i] + '0';
		output_character(c);
		i = i + 1;
	}
	output_character(' ');
}

void handle_operator(int op)
{
	int prec;
	if (op == ')') {
		while (top >= 0 && stack[top] != '(') {
			output_character(pop());
			output_character(' ');
		}
		if (top == -1) {
			printf("Unbalanced parenthesis\n");
			exit(5);
		}
		assert(pop() == '(');
		return;
	} else if (op == '(') {
		push('(');
	} else {
		prec = precedence(op);
		while (top > 0) {
			if (stack[top] == '(')
				break;
			if (prec > precedence(stack[top]))
				break;
			output_character(pop());
			output_character(' ');
		}
		push(op);
	}
}

int main(void)
{
	int len, t;
	top = -1;
	printf("Infix to Postfix conversion\n\n");
	printf("Enter infix expression: ");
	fgets(input, sizeof(input), stdin);
	len = strlen(input) - 1;
	input[len] = '\0';
	pos = 0;
	olen = 0;
	push('(');
	do {
		t = next_token();
		switch (toktype) {
		case T_ERROR:
			printf("Invalid character '%c' found while reading expression.\n", input[pos]);
			return 1;
		case T_END:
			t = ')';
		case T_OPERATOR:
			handle_operator(t);
			break;
		case T_NUMBER:
			output_number(t);
			break;
		default:
			/* unreachable */
			abort();
		}
	} while (toktype != T_END);
	if (top != -1) {
		printf("Unbalanced parenthesis\n");
		return 2;
	}
	output[olen] = '\0';
	printf("Corresponding postfix expression: %s\n", output);
	return 0;
}

