#include <stdio.h>

int main(void)
{
	int a[10][10], b[10][10], c[10][10], row, col, rowp, colp, i, j;
	printf("Matrix addition\n\n");
	printf("Enter the number of rows and columns in the first matrix: ");
	scanf("%i%i", &row, &col);
	printf("Enter the number of rows and columns in the second matrix: ");
	scanf("%i%i", &rowp, &colp);
	if (row != rowp || col != colp) {
		printf("Could not add matrices of different dimentions.\n");
		return 1;
	}
	printf("Enter %d elements in the first matrix:\n", row * col);
	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			scanf("%d", &a[i][j]);
		}
	}
	printf("Enter %d elements in the second matrix:\n", row * col);
	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			scanf("%d", &b[i][j]);
		}
	}
	puts("The elements of the first matrix are:\n");
	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			printf("%d\t", a[i][j]);
		}
		printf("\n");
	}
	puts("The elements of the second matrix are:\n");
	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			printf("%d\t", b[i][j]);
		}
		printf("\n");
	}
	/* addition */
	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			c[i][j] = a[i][j] + b[i][j];
		}
	}
	puts("The elements of the result matrix:\n");
	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			printf("%d\t", c[i][j]);
		}
		printf("\n");
	}
	return 0;
}

