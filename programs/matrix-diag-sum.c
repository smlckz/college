#include <stdio.h>

int main(void)
{
	int arr[10][10], row, col, sum, i, j;
	printf("Enter the number of rows and columns you want to enter: ");
	scanf("%i%i", &row, &col);
	if (row != col) {
		printf("Not a square matrix: problem of square matrix can not be solved\n");
		return 1;
	}
	printf("Enter the %d elements:\n", row * col);
	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			scanf("%d", &arr[i][j]);
		}
	}
	puts("The elements are:");
	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			printf("%d\t", arr[i][j]);
		}
		printf("\n");
	}
	sum = 0;
	puts("The upper diagonal elements are:");
	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			if (i == j) {
				printf("%d\t", arr[i][j]);
				sum += arr[i][j];
			}
		}
	}
	printf("\n");
	printf("The sum of all upper diagonal elements in the matrix is %d\n", sum);
	printf("The average of all upper diagonal elements in the matrix is %f\n", (float)sum / row);
	sum = 0;
	puts("The lower diagonal elements are:");
	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			if (i+j == col-1) {
				printf("%d\t", arr[j][i]);
				sum += arr[j][i];
			}
		}
	}
	printf("\n");
	printf("The sum of all lower diagonal elements in the matrix is %d\n", sum);
	printf("The average of all lower diagonal elements in the matrix is %f\n", (float)sum / row);
	return 0;
}

