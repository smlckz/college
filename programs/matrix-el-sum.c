#include <stdio.h>

int main(void)
{
	int arr[10][10], row, col, sum, i, j;
	printf("Enter the number of rows and columns you want to enter: ");
	scanf("%i%i", &row, &col);
	printf("Enter the %d elements:\n", row * col);
	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			scanf("%d", &arr[i][j]);
		}
	}
	puts("The elements are:");
	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			printf("%d\t", arr[i][j]);
		}
		printf("\n");
	}
	sum = 0;
	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			sum += arr[i][j];
		}
	}
	printf("The sum of all the elements in the matrix is %d\n", sum);
	printf("The average of all elements in the matrix is %f\n", (float)sum / (row * col));
	return 0;
}

