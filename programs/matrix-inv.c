#include <stdio.h>


float minor(float m[10][10], int len, int i, int j);

float determinant(float m[10][10], int len)
{
	int i, sign;
	float d;
	if (len == 1) {
		return m[0][0];
	}
	d = 0;
	sign = 1;
	for (i = 0; i < len; i++) {
		d = d + sign * m[0][i] * minor(m, len, 0, i);
		sign = sign * -1;
	}
	return d;
}

float minor(float x[10][10], int len, int i, int j)
{
	float t[10][10];
	int r, c, m, n;
	r = 0;
	for (m = 0; m < len; m++) {
		if (m == i) continue;
		c = 0;
		for (n = 0; n < len; n++) {
			if (n == j) continue;
			t[r][c] = x[m][n];
			c = c + 1;
		}
		r = r + 1;
	}
	return determinant(t, len - 1);
}

int main(void)
{
	float a[10][10], a_inv[10][10];
	int row, col, len, i, j, sign;
	float det, dinv;
	puts("Calculating matrix inverse\n");
	printf("Enter the number of rows and columns you want to enter in the matrix: ");
	scanf("%i%i", &row, &col);
	if (row != col) {
		printf("Could not calculate the inverse of a non-square matrix\n");
		return 1;
	}
	len = row;
	printf("Enter the %d elements for the matrix:\n", len * len);
	for (i = 0; i < len; i++) {
		for (j = 0; j < len; j++) {
			scanf("%f", &a[i][j]);
		}
	}
	det = determinant(a, len);
	if (det == 0.0f) {
		printf("Could not calculate inverse of a singular matrix\n");
		return 2;
	}
	dinv = 1.0f / det;
	/* calculate the inverse */
	for (i = 0; i < len; i++) {
		if (i % 2 == 0)
			sign = 1;
		else
			sign = -1;
		for (j = 0; j < len; j++) {
			a_inv[i][j] = sign * dinv * minor(a, len, j, i);
			sign = sign * -1;
		}
	}
	puts("The elements of the inverse matrix are:");
	for (i = 0; i < len; i++) {
		for (j = 0; j < len; j++) {
			printf("%g\t", a_inv[i][j]);
		}
		printf("\n");
	}
	return 0;
}

