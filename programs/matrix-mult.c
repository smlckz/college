#include <stdio.h>

int main(void)
{
	int a1[10][10], a2[10][10], a[10][10], m, n, p, pp, i, j, k;
	puts("Matrix multiplication\n");
	printf("Enter the number of rows and columns you want to enter in the first matrix: ");
	scanf("%i%i", &m, &p);
	printf("Enter the number of rows and columns you want to enter in the first matrix: ");
	scanf("%i%i", &pp, &n);
	if (p != pp) {
		printf("Could not mutiply these two matrices, the number of columns of first matrix and rows of second matrix must be equal.\n");
		return 1;
	}
	printf("Enter the %d elements for the first matrix:\n", m * p);
	for (i = 0; i < m; i++) {
		for (j = 0; j < p; j++) {
			scanf("%d", &a1[i][j]);
		}
	}
	printf("Enter the %d elements for the first matrix:\n", p * n);
	for (i = 0; i < p; i++) {
		for (j = 0; j < n; j++) {
			scanf("%d", &a2[i][j]);
		}
	}
	/* multiplication */
	for (i = 0; i < m; i++) {
		for (j = 0; j < n; j++) {
			a[i][j] = 0;
			for (k = 0; k < p; k++) {
				a[i][j] += a1[i][k] * a2[k][j];
			}
		}
	}
	puts("The elements of the multiplied matrix are:");
	for (i = 0; i < m; i++) {
		for (j = 0; j < n; j++) {
			printf("%d\t", a[i][j]);
		}
		printf("\n");
	}
	return 0;
}

