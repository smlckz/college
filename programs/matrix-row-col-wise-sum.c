#include <stdio.h>

int main(void)
{
	int arr[10][10], row, col, i, j, sum;
	printf("Enter the number of rows and columns you want to enter: ");
	scanf("%i%i", &row, &col);
	printf("Enter the %d elements:\n", row * col);
	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			scanf("%d", &arr[i][j]);
		}
	}
	printf("The elements of the matrix are:\n");
	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			printf("%d\t", arr[i][j]);
		}
		printf("\n");
	}
	puts("The row wise sums are: ");
	for (i = 0; i < row; i++) {
		sum = 0;
		for (j = 0; j < col; j++) {
			sum += arr[i][j];
		}
		printf(" %d", sum);
	}
	printf("\n");
	puts("The column wise sums are: ");
	for (j = 0; j < col; j++) {
		sum = 0;
		for (i = 0; i < row; i++) {
			sum += arr[i][j];
		}
		printf(" %d", sum);
	}
	printf("\n");
	return 0;
}

