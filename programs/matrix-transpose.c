#include <stdio.h>

int main(void)
{
	int arr[10][10], row, col, limit, i, j, t;
	printf("Enter the number of rows and columns you want to enter: ");
	scanf("%i%i", &row, &col);
	printf("Enter the %d elements:\n", row * col);
	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			scanf("%d", &arr[i][j]);
		}
	}
	puts("Before traspose, the elements are:");
	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			printf("%d\t", arr[i][j]);
		}
		printf("\n");
	}
	/* inner loop have to iterate max(row, col) times to complete transpose */
	if (row < col)
		limit = col;
	else
		limit = row;
	/* in-place transpose */
	for (i = 0; i < row; i++) {
		for (j = i; j < limit; j++) {
			/* swap arr[i][j] with arr[j][i] */
			t = arr[j][i];
			arr[j][i] = arr[i][j];
			arr[i][j] = t;
		}
	}
	t = row;
	row = col;
	col = t;
	printf("After transpose, the elements are:\n");
	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			printf("%d\t", arr[i][j]);
		}
		printf("\n");
	}
	return 0;
}

