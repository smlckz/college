#include <stdio.h>
#include <stdlib.h>

int read_array(int **arr)
{
	int i, len;
	printf("Enter the number of elements: ");
	scanf("%d", &len);
	*arr = malloc(len * sizeof(int));
	if (*arr == NULL) {
		printf("Could not allocate enough memory to hold the array\n");
		return -1;
	}
	printf("Enter the elements: ");
	for (i = 0; i < len; i++) {
		scanf("%d", *arr + i);
	}
	return len;
}

void print_array(int *arr, int n)
{
	int i;
	if (n == 0) {
		printf(" (empty array)");
	}
	for (i = 0; i < n; i++) {
		printf(" %d", arr[i]);
	}
	printf("\n");
}

#define SWAP(A, B) \
	((A) ^= (B), (B) ^= (A), (A) ^= (B))

void bubble_sort(int *arr, int len)
{
	int bound = len, i, t;
	while (bound > 1) {
		t = 0;
		for (i = 0; i < bound; i++) {
			if (arr[i] > arr[i+1]) {
				SWAP(arr[i], arr[i+1]);
				t = i;
			}
		}
		if (t == 0) break;
		bound = t;
	}
}

int main(void)
{
	int *arr, len;
	len = read_array(&arr);
	if (len == -1) return 1;
	printf("Before sorting, the array elements are: ");
	print_array(arr, len);
	bubble_sort(arr, len);
	printf("After sorting, the array elements are: ");
	print_array(arr, len);
	return 0;
}

