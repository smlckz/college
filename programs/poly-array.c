#include <stdio.h>
#include <stdlib.h>

#define POLY_MAX 10

int read_poly(int *poly)
{
	int i, degree;
	printf("Enter the degree of the polynomial: ");
	scanf("%d", &degree);
	if (degree < 0) {
		printf("Invalid degree %d for polynomial, must be positive\n", degree);
		return -1;
	} else if (degree >= POLY_MAX) {
		printf("Unsupported degree of polynomial, must be less than %d\n", POLY_MAX);
		return -1;
	}
	printf("Enter %d coefficients: ");
	for (i = 0; i <= degree; i++) {
		scanf("%d", &poly[i]);
	}
	return degree;
}

int main(void)
{
	int poly1[POLY_MAX], poly2[POLY_MAX], poly3[2 * POLY_MAX];
	int poly1degree, poly2degree, poly3degree;
	int choice;
	printf("Menu driven program for operations on polynomials\n\n");
	printf("NOTE: You have to enter the coefficients in the ascending order of the power of the variable of the polynomial.\n");
	while (1) {
		printf("Enter:\n"
		       "1. Add\n"
		       "2. Subtract\n"
		       "3. Multiply\n"
		       "4. Exit\n\n");
		printf("Enter your choice: ");
		scanf("%d", &choice);
		switch (choice) {
		case 1:
			printf("Addition of two polynomials\n");
		case 4:
			printf("Bye.\n");
			return 0;
		default:
			printf("");
		}
	}
	return 0;
}

