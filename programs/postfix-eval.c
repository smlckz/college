#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STACK_MAX 100

int stack[STACK_MAX];
int top;

void push(int item)
{
	if (top == STACK_MAX - 1) {
		puts("Stack overflow!");
		exit(1);
	}
	top = top + 1;
	stack[top] = item;
}

int pop(void)
{
	int item;
	if (top == -1) {
		puts("Stack underflow!");
		exit(2);
	}
	item = stack[top];
	top = top - 1;
	return item;
}

int main(void)
{
	char input[100], *p;
	int len, i, n, b, a, c;
	top = -1;
	printf("Enter postfix expression to be evaluated: ");
	fgets(input, sizeof(input), stdin);
	len = strlen(input) - 1;
	input[len] = '\0';
	for (i = 0; i < len; i++) {
		c = input[i];
		if (c == ' ' || c == '\t')
			continue;
		if (c == '+') {
			b = pop();
			a = pop();
			push(a + b);
		} else if (c == '-') {
			b = pop();
			a = pop();
			push(a - b);
		} else if (c == '*') {
			b = pop();
			a = pop();
			push(a * b);
		} else if (c == '/') {
			b = pop();
			if (b == 0) {
				puts("Can not divide by zero!");
				exit(3);
			}
			a = pop();
			push(a - b);
		} else {
			n = strtol(&input[i], &p, 10);
			if (p == input + i) {
				printf("Not a number or operator: `%c`\n",
					input[i]);
				exit(4);
			}
			i += p - (input + i) - 1;
			push(n);
		}
	}
	if (top != 0) {
		printf("More than one element left on the stack after evaluation.\n");
	}
	printf("Result: %d\n", pop());
	return 0;
}

