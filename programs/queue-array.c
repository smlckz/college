#include <stdio.h>
#include <stdlib.h>

struct queue {
	int *buf;
	int front, rear, max;
};

int queue_init(struct queue *q, int max)
{
	q->buf = malloc(max * sizeof(q->buf[0]));
	if (q->buf == NULL)
		return -1;
	q->max = max;
	q->front = -1;
	q->rear = -1;
	return 0;
}

int enqueue(struct queue *q, int item)
{
	if (q->rear == q->max - 1) {
		return -1;
	}
	q->rear = q->rear + 1;
	q->buf[q->rear] = item;
	if (q->front == -1) {
		q->front = 0;
	}
	return 0;
}

int dequeue(struct queue *q, int *item)
{
	if (q->front == -1) {
		return -1;
	}
	*item = q->buf[q->front];
	q->front = q->front + 1;
	if (q->front > q->rear) {
		q->front = q->rear = -1;
	}
	return 0;
}

void display(struct queue *q)
{
	int i;
	if (q->front == -1) {
		printf("Queue is empty.\n");
		return;
	}
	printf("The elements of the queue are: ");
	for (i = q->front; i <= q->rear; i++) {
		printf("%d ", q->buf[i]);
	}
	printf("\n");
}

void queue_free(struct queue *q)
{
	free(q->buf);
}

int main(void)
{
	struct queue q;
	int choice, item, maxsize;
	printf("Queue of integers implemented using array\n\n");
	printf("Enter the maximum size of the queue: ");
	scanf("%d", &maxsize);
	if (queue_init(&q, maxsize)) {
		printf("Could not allocate enough memeory.\n");
		return 1;
	}
	while (1) {
		printf("Enter: \n"
			"1. Enqueue\n"
			"2. Dequeue\n"
			"3. Display\n"
			"4. Exit\n\n");
		printf("Enter your choice: ");
		scanf("%d", &choice);
		switch (choice) {
		case 1:
			if (q.rear == q.max - 1) {
				printf("Queue is full, can not insert\n");
				break;
			}
			printf("Enter an element to enqueue: ");
			scanf("%d", &item);
			enqueue(&q, item);
			display(&q);
			break;
		case 2:
			if (q.front == -1) {
				printf("Empty queue, can not delete\n");
				break;
			}
			dequeue(&q, &item);
			printf("The element thus dequeued is %d\n", item);
			display(&q);
			break;
		case 3:
			display(&q);
			break;
		case 4:
			puts("Bye\n");
			queue_free(&q);
			return 0;
		}
	}
	return 0;
}

