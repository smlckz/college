#include <stdio.h>
#include <stdlib.h>

int read_array(int **arr)
{
	int i, len;
	printf("Enter the number of elements: ");
	scanf("%d", &len);
	*arr = malloc(len * sizeof(int));
	if (*arr == NULL) {
		printf("Could not allocate enough memory to hold the array\n");
		return -1;
	}
	printf("Enter the elements: ");
	for (i = 0; i < len; i++) {
		scanf("%d", *arr + i);
	}
	return len;
}

void print_array(int *arr, int n)
{
	int i;
	if (n == 0) {
		printf(" (empty array)");
	}
	for (i = 0; i < n; i++) {
		printf(" %d", arr[i]);
	}
	printf("\n");
}

void swap(int *a, int *b)
{
	int tmp = *b;
	*b = *a;
	*a = tmp;
}

static int array_min(int *arr, int len)
{
	int minidx = 0, i;
	if (len == 0) return -1;
	for (i = 1; i < len; i++) {
		if (arr[i] < arr[minidx])
			minidx = i;
	}
	return minidx;
}

void selection_sort(int *arr, int len)
{
	int i, j, minidx;
	for (i = 0; i < len - 1; i++) {
		minidx = array_min(arr + i, len - i);
		if (minidx != 0)
			swap(arr + i, arr + i + minidx);
	}
}

int main(void)
{
	int *arr, len;
	len = read_array(&arr);
	if (len == -1) return 1;
	printf("Before sorting, the array elements are: ");
	print_array(arr, len);
	selection_sort(arr, len);
	printf("After sorting, the array elements are: ");
	print_array(arr, len);
	return 0;
}

