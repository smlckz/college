#include <stdio.h>

#define SORTLIB_IMPL
#define VERBOSE
#include "sortlib.h"

int main(void)
{
	int i;
	int *arr;
	int len;
	int choice;
	struct sorting_algorithm *algos = sorting_algorithms;
	printf("Sorting array using different algorithms\n\n");
	while (1) {
		while (1) {
			printf("Choose the sorting algorithm\n");
			for (i = 0; algos[i].name != NULL; i++) {
				printf("% 3d. %s\n", i + 1, algos[i].name);
			}
			printf("% 3d. Exit\n\n", i + 1);
			printf("Enter your choice: ");
			scanf("%d", &choice);
			if (choice == i + 1) {
				printf("Bye\n");
				return 0;
			}
			choice--;
			if (choice > -1 && choice < i) {
				break;
			}
			printf("Invalid choice. Try again.\n");
		}
		printf("Enter the array to be sorted\n");
		len = read_array(&arr);
		if (len < 0) {
			printf("Could not read the array. Try Again.\n");
			continue;
		}
		printf("Before sorting, the array elements are: ");
		print_array(arr, len);
		algos[choice].f(arr, len);
		printf("After sorting, the array elements are: ");
		print_array(arr, len);
		free(arr);
	}
	return 0;
}

