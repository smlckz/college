#ifndef SORTLIB_H
#define SORTLIB_H

int read_array(int **);
void print_array(int *, int);
void swap(int *, int *);

#ifdef VERBOSE
#define ARRAY_PASS(arr, len, pass) do { \
	printf("Pass %i: ", (pass)); \
	print_array((arr), (len)); \
	} while (0)
#else
#define ARRAY_PASS(arr, len, pass) (void)0
#endif

typedef void (*sort_func)(int *, int);

struct sorting_algorithm {
	char *name;
	sort_func f;
};

extern struct sorting_algorithm *sorting_algorithms;

#ifdef SORTLIB_IMPL

#include <stdlib.h>

void swap(int *a, int *b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

int read_array(int **arr)
{
	int i, len;
	printf("Enter the number of elements: ");
	scanf("%d", &len);
	*arr = malloc(len * sizeof(int));
	if (*arr == NULL) {
		printf("Could not allocate enough memory to hold the array\n");
		return -1;
	}
	printf("Enter the elements: ");
	for (i = 0; i < len; i++) {
		scanf("%d", *arr + i);
	}
	return len;
}

void print_array(int *arr, int n)
{
	int i;
	if (n == 0) {
		printf(" (empty array)");
	}
	for (i = 0; i < n; i++) {
		printf(" %d", arr[i]);
	}
	printf("\n");
}

void bubble_sort(int *arr, int len)
{
	int i, j;
	for (i = 0; i < len - 1; i++) {
		for (j = 0; j < len - i - 1; j++) {
			if (arr[j] > arr[j+1]) {
				swap(&arr[j], &arr[j+1]);
			}
		}
		ARRAY_PASS(arr, len, i+1);
	}
}

void insertion_sort(int *arr, int len)
{
	int i, j, tmp;
	for (i = 1; i < len; i++) {
		tmp = arr[i];
		for (j = i - 1; j >= 0 && tmp < arr[j]; j--) {
			arr[j + 1] = arr[j];
		}
		arr[j + 1] = tmp;
		ARRAY_PASS(arr, len, i);
	}
}

static int array_min(int *arr, int len)
{
	int minidx = 0, i;
	if (len == 0) return -1;
	for (i = 1; i < len; i++) {
		if (arr[i] < arr[minidx])
			minidx = i;
	}
	return minidx;
}

void selection_sort(int *arr, int len)
{
	int i, j, minidx;
	for (i = 0; i < len - 1; i++) {
		minidx = array_min(arr + i, len - i);
		if (minidx != 0)
			swap(arr + i, arr + i + minidx);
		ARRAY_PASS(arr, len, i+1);
	}
}

struct sorting_algorithm implemented_sorting_algorithms[] = {
	{ "Bubble Sort", bubble_sort },
	{ "Selection Sort", selection_sort },
	{ "Insertion Sort", insertion_sort },
	{ NULL, 0 },
};

struct sorting_algorithm *sorting_algorithms = implemented_sorting_algorithms;

#endif

#endif

