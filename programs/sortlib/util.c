#include "sortlib.h"

#include <stdio.h>
#include <stdlib.h>

int read_array(int **arr)
{
	int i, len;
	printf("Enter the number of elements: ");
	scanf("%d", &len);
	*arr = malloc(len * sizeof(int));
	if (*arr == NULL) {
		printf("Could not allocate enough memory to hold the array\n");
		return -1;
	}
	printf("Enter the elements: ");
	for (i = 0; i < len; i++) {
		scanf("%d", *arr + i);
	}
	return len;
}

void print_array(int *arr, int n)
{
	int i;
	if (n == 0) {
		printf(" (empty array)");
	}
	for (i = 0; i < n; i++) {
		printf(" %d", arr[i]);
	}
	printf("\n");
}

void swap(int *a, int *b)
{
	int tmp = *b;
	*b = *a;
	*a = tmp;
}

