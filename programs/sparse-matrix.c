#include <stdio.h>

int main(void)
{
	int m[20][20], s[3][50];
	int i, j, k, row, col, nonzero;
	printf("Enter the number of rows and columns of the matrix: ");
	scanf("%i%i", &row, &col);
	printf("Enter %d elements of the matrix: ", row * col);
	nonzero = 0;
	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			scanf("%d", &m[i][j]);
			if (m[i][j] != 0) nonzero++;
		}
	}
	if (nonzero * 3 >= row * col) {
		printf("Not a sparse matrix\n");
		return 1;
	}
	/* fill the array representing sparse matrix */
	k = 0;
	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			if (m[i][j] != 0) {
				s[0][k] = i;
				s[1][k] = j;
				s[2][k] = m[i][j];
				k = k + 1;
			}
		}
	}
	/* print the sparse matrix */
	k = 0;
	printf("The matrix elements are:\n");
	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			if (s[0][k] == i && s[1][k] == j) {
				printf("%d\t", s[2][k]);
				k = k + 1;
			} else {
				printf("0\t");
			}
		}
		printf("\n");
	}
}

