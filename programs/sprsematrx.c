#include <stdio.h>

#define MAX 50

struct matrix_cell {
	int row, col;
	int data;
};

void insert(struct matrix_cell *m, int len, struct matrix_cell newcell)
{
	int i = len - 1;
	while (i >= 0) {
		if (m[i].row <= newcell.row) break;
		if (m[i].row == newcell.row && m[i].col <= newcell.col) break;
		m[i + 1] = m[i];
		i = i - 1;
	}
	m[i + 1] = c;
}

int main(void)
{
	struct matrix_cell m[MAX], cell;
	int row, col, len, i, j, k;
	printf("Sparse Matrix\n\n");
	printf("Enter the number of rows and columns of the matrix: ");
	scanf("%i%i", &row, &col);
	printf("Enter the number of elements: ");
	scanf("%d", &len);
	if (len >= MAX) {
		printf("Unsupported number of elements, maximum is %d\n", MAX);
		return 1;
	} else if (len > row * col) {
		printf("Invalid number of elements %d for a %d x %d matrix\n", len, row, col);
		return 2;
	}
	for (i = 0; i < len; i++) {
		printf("Enter row, column and value: ");
		scanf("%d%d%d", &cell.row, &cell.col, &cell.data);
		if (cell.row > row || cell.col > col) {
			printf("Indices %d,%d out of bounds for %d x %d matrix\n",
				cell.row, cell.col, row, col);
			printf("Try again\n");
			i = i - 1;
			continue;
		}
		insert(m, i, cell);
	}
	printf("The elements of the matrix are: ");
	k = 0;
	for (i = 0; i < row; i++) {
		for (j = 0; j < col; j++) {
			cell = m[i][j];
			if (cell.row == i && cell.col == j) {
				printf("%d", cell.data);
				k = k + 1;
			}
			printf("\t");
		}
		printf("\n");
	}
	return 0;
}

