#include <stdio.h>

int push(int *stack, int max, int top)
{
	int item;
	if (top == max - 1) {
		printf("Stack overflow.\n");
	} else {
		printf("Enter the element to be pushed: ");
		scanf("%d%*c", &item);
		top = top + 1;
		stack[top] = item;
	}
	return top;
}

int pop(int *stack, int top)
{
	int item;
	if (top == -1) {
		printf("Stack underflow.\n");
	} else {
		item = stack[top];
		top = top - 1;
	}
	return top;
}

void display(int *stack, int top)
{
	int i;
	if (top == -1) {
		printf("Stack is empty.\n");
		return;
	}
	printf("Elements of the stack:\n");
	for (i = top; i >= 0; i--) {
		printf(" %d\n", stack[i]);
	}
	printf("\n");
}

int main(void)
{
	int stack[50], max, choice, item, top, i;
	printf("Enter the size of the stack: ");
	scanf("%d", &max);
	top = -1;
	while (1) {
		printf("Enter:\n"
			"1. push\n"
			"2. pop\n"
			"3. display\n"
			"4. exit\n");
		printf("Enter your choice: ");
		scanf("%d", &choice);
		switch (choice) {
		case 1:
			top = push(stack, max, top);
			display(stack, top);
			break;
		case 2:
			top = pop(stack, top);
			display(stack, top);
			break;
		case 3:
			display(stack, top);
			break;
		case 4:
			printf("Bye.\n");
			return 0;
		default:
			printf("Invalid choice, try again.\n");
		}
	}
	return 0;
}

